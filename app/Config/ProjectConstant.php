<?php
namespace App\Config;

class ProjectConstant
{
    const API_VERSION = '1.1';

    const Frequencies = [
        'YEAR', 'MONTH', 'WEEK', 'DAY'
    ];
    
    const Page = 1;
    const PageSize = 10000;
    const MaxPageSize = 500;
    const OrderType = 'desc';

    public static $month = [
        'eng' => [
            '1' => 'January',
            '2' => 'February',
            '3' => 'March',
            '4' => 'April',
            '5' => 'May',
            '6' => 'June',
            '7' => 'July',
            '8' => 'August',
            '9' => 'September',
            '10' => 'October',
            '11' => 'November',
            '12' => 'December'
        ],
        'urd' => [
            '1' => 'جنوری',
            '2' => 'فروری',
            '3' => 'مارچ',
            '4' => 'اپریل',
            '5' => 'مئی',
            '6' => 'جون',
            '7' => 'جولائ',
            '8' => 'اگست',
            '9' => 'ستمبر',
            '10' => 'اکتوبر',
            '11' => 'نومبر',
            '12' => 'دسمبر'
        ]
    ];
}