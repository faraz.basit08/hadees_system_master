<?php

namespace App\Http\Controllers;

use App\Config\ProjectConstant;
use App\Models\SetupSubTopic;
use DiUtil\Config\Constant;
use DiUtil\Utilities\Utilities;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller;

class SetupSubTopicAPI extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    private $select_columns = [
        'id',
        'topic_id',
        'hds_book_id',
        'volume_id',
        'chap_id',
        'sub_topic_name',
        'is_enable as activate',
    ];

    /**
     * This fucntion is called after validation fails in function $this->validate.
     *
     * @param Request $request
     * @param array $errors
     * @return \Illuminate\Http\JsonResponse
     */

    protected function buildFailedValidationResponse(Request $request, array $errors)
    {
        $response = Utilities::buildFailedValidationResponse(10000, "Unprocesssable Entity.", $errors);
        return response()->json($response, 400);
    }

    /**
     * Add Role.
     *
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function addSetupSubTopic(Request $request)
    {
        $mdl = new SetupSubTopic();

        $this->validate($request, $mdl->rules($request), $mdl->messages($request));

        $mdl->filterColumns($request);

        Utilities::defaultAddAttributes($request);

        $item = SetupSubTopic::create($request->all());

        $data = ['id' => $item->id];

        $response = Utilities::buildSuccessResponse(1070, "Sub Topic successfully created.", $data);

        return response()->json($response, 201);
    }

    /**
     * Update Role.
     *
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function updateSetupSubTopic(Request $request)
    {
        $mdl = new SetupSubTopic();

        $this->validate($request, $mdl->rules($request), $mdl->messages($request));

        $mdl->filterColumns($request);

        Utilities::defaultUpdateAttributes($request);

        $item = SetupSubTopic::find($request->id);


        if (!$item) {
            $status = 404;
            $response = Utilities::buildBaseResponse(1074, "Sub Topic not found.");
        } else {
            $status = 200;

            $obj = $item->update($request->all());

            if ($obj) {
                $data = ['id' => $item->id];
                $response = Utilities::buildSuccessResponse(1071, "Sub Topic successfully updated.", $data);
            }
        }

        return response()->json($response, $status);
    }

    /**
     * Activate/De-Activate Role.
     *
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function patchDisableEnableSetupSubTopic(Request $request)
    {
        $mdl = new SetupSubTopic();

        Utilities::removeAttributesExcept($request, ["id", "activate"]);

        $this->validate($request, $mdl->rules($request), $mdl->messages($request));

        Utilities::defaultUpdateAttributes($request);

        $activate = $request->activate == 1 ? Constant::RecordType['ENABLED'] : Constant::RecordType['DISABLED'];

        $request->request->add(['is_enable' => $activate]);

        $item = SetupSubTopic::find($request->id);

        $response = [];

        if (!$item) {
            $status = 404;
            $response = Utilities::buildBaseResponse(1074, "Sub Topic not found.");
        } else {
            $status = 200;

            $obj = $item->update($request->all());

            if ($obj) {
                $data = ['id' => $item->id];
                $actMsg = $request->activate == 1 ? "activated" : "de-activated";
                $response = Utilities::buildSuccessResponse(1072, "Sub Topic successfully $actMsg.", $data);
            }
        }

        return response()->json($response, $status);
    }

    /**
     * Delete Role.
     *
     * @param $id 'ID' of Role to delete. (required)
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function deleteSetupSubTopic($id, Request $request)
    {
        $mdl = new SetupSubTopic();

        $request->request->add(['id' => $id]);

        $this->validate($request, $mdl->rules($request), $mdl->messages($request));

        Utilities::defaultDeleteAttributes($request);

        $request->request->add(['is_enable' => Constant::RecordType['DELETED']]);

        $item = SetupSubTopic::find($request->id);

        if (!$item) {
            $status = 404;
            $response = Utilities::buildBaseResponse(1074, "Sub Topic not found.");
        } else {
            $status = 200;

            $obj = $item->update($request->all());

            if ($obj) {
                $response = Utilities::buildBaseResponse(1073, "Sub Topic successfully deleted.");
            }
        }

        return response()->json($response, $status);
    }

    /**
     * Get one Role.
     *
     * @param $id 'ID' of Role to return (required)
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function getOneSetupSubTopic($id, Request $request)
    {
        $mdl = new SetupSubTopic();

        $request->request->add(['id' => $id]);

        $this->validate($request, $mdl->rules($request, Constant::RequestType['GET_ONE']), $mdl->messages($request, Constant::RequestType['GET_ONE']));

        $select = $this->select_columns;

        $mdl->filterColumns($request);

        if ($request->fields) {
            $select = $request->fields;
        }

        $item = SetupSubTopic::where('id', $request->id)->first($select);

        if (!$item) {
            $status = 404;
            $response = Utilities::buildBaseResponse(1074, "Sub Topic not found.");
        } else {
            $status = 200;
            $dataResult = array("setup_sub_topic" => $item->toArray());
            $response = Utilities::buildSuccessResponse(1075, "Sub Topic data.", $dataResult);
        }

        return response()->json($response, $status);
    }

    /**
     * Fetch list of Role by searching with optional filters..
     *
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function getAllSetupSubTopic(Request $request)
    {
        $mdl = new SetupSubTopic();

        $this->validate($request, $mdl->rules($request, Constant::RequestType['GET_ALL']), $mdl->messages($request, Constant::RequestType['GET_ALL']));

        $pageSize = $request->limit ?? ProjectConstant::PageSize;
        if ($pageSize > ProjectConstant::MaxPageSize) {
            $pageSize = ProjectConstant::PageSize;
        }
        $page = $request->page ?? ProjectConstant::Page;
        $skip = ($page - 1) * $pageSize;


        $select = $this->select_columns;

        $mdl->filterColumns($request);

        if ($request->fields) {
            $select = $request->fields;
        }

        $whereData = array();
        if ($request->sub_topic_name) {
            $whereData[] = ['sub_topic_name', 'LIKE', "%{$request->sub_topic_name}%"];
        }
        if ($request->topic_id) {
            $whereData[] = ['topic_id', '=', $request->topic_id];
        }
        if ($request->activate != null) {
            $whereData[] = ['is_enable', $request->activate];
        }

        $itemCount = SetupSubTopic::with('topic')->where($whereData)->active()->get()->count();

        $orderBy = $mdl->getOrderColumn($request->order_by);
        $orderType = $request->order_type ?? ProjectConstant::OrderType;

        $items = SetupSubTopic::with('topic')->where($whereData)
            ->active()
            ->orderBy($orderBy, $orderType)
            ->offset($skip)
            ->limit($pageSize)
            ->get($select);

        $status = 200;
        $data_result = array("setup_sub_topics_total" => $itemCount, "setup_sub_topics" => $items->toArray());
        $response = Utilities::buildSuccessResponse(1076, "Sub Topic list.", $data_result);

        return response()->json($response, $status);
    }

}
