<?php

namespace App\Http\Controllers;

use App\Config\ProjectConstant;
use App\Models\SetupLanguage;
use App\Models\UserRoles;
use App\Response\LanguageResponse;
use App\Response\UserRolesResponse;
use DiUtil\Config\Constant;
use DiUtil\Utilities\Utilities;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller;

class SetupLanguageApi extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    private $select_columns = ['id', 'language_name', 'is_enable as activate'];

    /**
     * This fucntion is called after validation fails in function $this->validate.
     *
     * @param Request $request
     * @param array $errors
     * @return \Illuminate\Http\JsonResponse
     */

    protected function buildFailedValidationResponse(Request $request, array $errors){
        $response = Utilities::buildFailedValidationResponse(10000, "Unprocesssable Entity.", $errors);
        return response()->json($response, 400);
    }
    /**
     * Add Role.
     *
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function addSetupLanguage(Request $request)
    {
        $mdlSetupLanguage = new SetupLanguage();

        $this->validate($request, $mdlSetupLanguage->rules($request), $mdlSetupLanguage->messages($request));

        $mdlSetupLanguage->filterColumns($request);

        Utilities::defaultAddAttributes($request);

        $obj = SetupLanguage::create($request->all());

        $data = ['id' => $obj->id];

        $response = Utilities::buildSuccessResponse(1070, "Language successfully created.", $data);

        return response()->json($response, 201);
    }

    /**
     * Update Language.
     *
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateSetupLanguage(Request $request)
    {
        $mdlSetupLanguage = new SetupLanguage();

        $this->validate($request, $mdlSetupLanguage->rules($request), $mdlSetupLanguage->messages($request));

        $mdlSetupLanguage->filterColumns($request);

        Utilities::defaultUpdateAttributes($request);

        $language = SetupLanguage::find($request->id);

        $status = 200;
        $response = [];

        if (!$language) {
            $status = 404;
            $response = Utilities::buildBaseResponse(1074, "Language not found.");
        } else {

            $obj = $language->update($request->all());

            if ($obj) {
                $data = ['id' => $language->id];
                $response = Utilities::buildSuccessResponse(1071, "Language successfully updated.", $data);
            }
        }

        return response()->json($response, $status);
    }

    /**
     * Activate/De-Activate Role.
     *
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function patchDisableEnableSetupLanguage(Request $request)
    {
        $mdlSetupLanguage = new SetupLanguage();

        Utilities::removeAttributesExcept($request, ["id", "activate"]);

        $this->validate($request, $mdlSetupLanguage->rules($request), $mdlSetupLanguage->messages($request));

        Utilities::defaultUpdateAttributes($request);

        $activate = $request->activate == 1 ? Constant::RecordType['ENABLED'] : Constant::RecordType['DISABLED'];

        $request->request->add(['is_enable' => $activate]);

        $SetupLanguage = SetupLanguage::find($request->id);

        $status = 200;
        $response = [];

        if (!$SetupLanguage) {
            $status = 404;
            $response = Utilities::buildBaseResponse(1074, "Language not found.");
        } else {

            $obj = $SetupLanguage->update($request->all());

            if ($obj) {
                $data = ['id' => $SetupLanguage->id];
                $actMsg = $request->activate == 1 ? "activated" : "de-activated";
                $response = Utilities::buildSuccessResponse(1072, "Language successfully $actMsg.", $data);
            }
        }

        return response()->json($response, $status);
    }

    /**
     * Delete Role.
     *
     * @param $id 'ID' of Role to delete. (required)
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteSetupLanguage($id, Request $request)
    {
        $mdlSetupLanguage = new SetupLanguage();

        $request->request->add(['id' => $id]);

        $this->validate($request, $mdlSetupLanguage->rules($request), $mdlSetupLanguage->messages($request));

        Utilities::defaultDeleteAttributes($request);

        $request->request->add(['is_enable' => Constant::RecordType['DELETED']]);

        $SetupLanguage = SetupLanguage::find($request->id);

        $status = 200;
        $response = [];
        if (!$SetupLanguage) {
            $status = 404;
            $response = Utilities::buildBaseResponse(1074, "Language not found.");
        } else {

            $obj = $language->update($request->all());
            if ($obj) {
                $response = Utilities::buildBaseResponse(1073, "Language successfully deleted.");
            }
        }

        return response()->json($response, $status);
    }

    /**
     * Get one Role.
     *
     * @param $id 'ID' of Role to return (required)
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getOneSetupLanguage($id, Request $request)
    {
        $mdlSetupLanguage = new SetupLanguage();

        $request->request->add(['id' => $id]);

        $this->validate($request, $mdlSetupLanguage->rules($request, Constant::RequestType['GET_ONE']), $mdlSetupLanguage->messages($request, Constant::RequestType['GET_ONE']));

        $select = $this->select_columns;

        $mdlSetupLanguage->filterColumns($request);

        if ($request->fields) {
            $select = $request->fields;
        }

        $SetupLanguage = SetupLanguage::where('id', $request->id)->first($select);
        $status = 200;
        $response = [];

        if (!$SetupLanguage) {
            $status = 404;
            $response = Utilities::buildBaseResponse(1074, "Language not found.");
        } else {
            $status = 200;
            $dataResult = array("language" => $SetupLanguage->toArray());
            $response = Utilities::buildSuccessResponse(1075, "Language data.", $dataResult);
        }

        return response()->json($response, $status);
    }

    /**
     * Fetch list of Role by searching with optional filters..
     *
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllSetupLanguage(Request $request)
    {
        $mdlSetupLanguage = new SetupLanguage();

        $this->validate($request, $mdlSetupLanguage->rules($request, Constant::RequestType['GET_ALL']), $mdlSetupLanguage->messages($request, Constant::RequestType['GET_ALL']));

        $pageSize = $request->limit ?? ProjectConstant::PageSize;
        if ($pageSize > ProjectConstant::MaxPageSize) {
            $pageSize = ProjectConstant::PageSize;
        }
        $page = $request->page ?? ProjectConstant::Page;
        $skip = ($page - 1) * $pageSize;


        $select = $this->select_columns;

        $mdlSetupLanguage->filterColumns($request);

        if ($request->fields) {
            $select = $request->fields;
        }

        $whereData = array();
        if ($request->language_name) {
            $whereData[] = ['language_name', 'LIKE', "%{$request->language_name}%"];
        }
        if ($request->activate != null) {
            $whereData[] = ['is_enable', $request->activate];
        }

        $SetupLanguageCount = SetupLanguage::where($whereData)->active()->get()->count();

        $orderBy =  $mdlSetupLanguage->getOrderColumn($request->order_by);
        $orderType = $request->order_type ?? ProjectConstant::OrderType;

        $SetupLanguage = SetupLanguage::where($whereData)
        ->active()
        ->orderBy($orderBy, $orderType)
        ->offset($skip)
        ->limit($pageSize)
        ->get($select);

        $status = 200;
        $data_result = array("language_total" => $SetupLanguageCount, "language" => $SetupLanguage->toArray());
        $response = Utilities::buildSuccessResponse(1076, "Language list.", $data_result);

        return response()->json($response, $status);
    }

}
