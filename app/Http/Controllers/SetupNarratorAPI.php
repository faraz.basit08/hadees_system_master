<?php

namespace App\Http\Controllers;

use App\Config\ProjectConstant;
use App\Models\SetupNarrator;
use DiUtil\Config\Constant;
use DiUtil\Utilities\Utilities;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller;

class SetupNarratorAPI extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    private $select_columns = [
        'id',
        'tabqa_id',
        'nratr_name',
        'father_name',
        'kuniyat',
        'nasab',
        'laqab',
        'nratr_dob',
        'nratr_pob',
        'nratr_dod',
        'nratr_pod',
        'nratr_teachers',
        'nratr_student',
        'is_enable as activate',
    ];

    /**
     * This fucntion is called after validation fails in function $this->validate.
     *
     * @param Request $request
     * @param array $errors
     * @return \Illuminate\Http\JsonResponse
     */

    protected function buildFailedValidationResponse(Request $request, array $errors)
    {
        $response = Utilities::buildFailedValidationResponse(10000, "Unprocesssable Entity.", $errors);
        return response()->json($response, 400);
    }

    /**
     * Add Role.
     *
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function addSetupNarrator(Request $request)
    {
        $mdl = new SetupNarrator();
        $this->validate($request, $mdl->rules($request), $mdl->messages($request));

        $mdl->filterColumns($request);

        Utilities::defaultAddAttributes($request);

        $item = SetupNarrator::create($request->all());

        $data = ['id' => $item->id];

        $response = Utilities::buildSuccessResponse(1070, "Narrator successfully created.", $data);

        return response()->json($response, 201);
    }

    /**
     * Update Role.
     *
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function updateSetupNarrator(Request $request)
    {
        $mdl = new SetupNarrator();

        $this->validate($request, $mdl->rules($request), $mdl->messages($request));

        $mdl->filterColumns($request);

        Utilities::defaultUpdateAttributes($request);

        $item = SetupNarrator::find($request->id);


        if (!$item) {
            $status = 404;
            $response = Utilities::buildBaseResponse(1074, "Narrator not found.");
        } else {
            $status = 200;

            $obj = $item->update($request->all());

            if ($obj) {
                $data = ['id' => $item->id];
                $response = Utilities::buildSuccessResponse(1071, "Narrator successfully updated.", $data);
            }
        }

        return response()->json($response, $status);
    }

    /**
     * Activate/De-Activate Role.
     *
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function patchDisableEnableSetupNarrator(Request $request)
    {
        $mdl = new SetupNarrator();

        Utilities::removeAttributesExcept($request, ["id", "activate"]);

        $this->validate($request, $mdl->rules($request), $mdl->messages($request));

        Utilities::defaultUpdateAttributes($request);

        $activate = $request->activate == 1 ? Constant::RecordType['ENABLED'] : Constant::RecordType['DISABLED'];

        $request->request->add(['is_enable' => $activate]);

        $item = SetupNarrator::find($request->id);

        $response = [];

        if (!$item) {
            $status = 404;
            $response = Utilities::buildBaseResponse(1074, "Narrator not found.");
        } else {
            $status = 200;

            $obj = $item->update($request->all());

            if ($obj) {
                $data = ['id' => $item->id];
                $actMsg = $request->activate == 1 ? "activated" : "de-activated";
                $response = Utilities::buildSuccessResponse(1072, "Narrator successfully $actMsg.", $data);
            }
        }

        return response()->json($response, $status);
    }

    /**
     * Delete Role.
     *
     * @param $id 'ID' of Role to delete. (required)
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function deleteSetupNarrator($id, Request $request)
    {
        $mdl = new SetupNarrator();

        $request->request->add(['id' => $id]);

        $this->validate($request, $mdl->rules($request), $mdl->messages($request));

        Utilities::defaultDeleteAttributes($request);

        $request->request->add(['is_enable' => Constant::RecordType['DELETED']]);

        $item = SetupNarrator::find($request->id);

        if (!$item) {
            $status = 404;
            $response = Utilities::buildBaseResponse(1074, "Narrator not found.");
        } else {
            $status = 200;

            $obj = $item->update($request->all());

            if ($obj) {
                $response = Utilities::buildBaseResponse(1073, "Narrator successfully deleted.");
            }
        }

        return response()->json($response, $status);
    }

    /**
     * Get one Role.
     *
     * @param $id 'ID' of Role to return (required)
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function getOneSetupNarrator($id, Request $request)
    {
        $mdl = new SetupNarrator();

        $request->request->add(['id' => $id]);

        $this->validate($request, $mdl->rules($request, Constant::RequestType['GET_ONE']), $mdl->messages($request, Constant::RequestType['GET_ONE']));

        $select = $this->select_columns;

        $mdl->filterColumns($request);

        if ($request->fields) {
            $select = $request->fields;
        }

        $item = SetupNarrator::where('id', $request->id)->first($select);

        if (!$item) {
            $status = 404;
            $response = Utilities::buildBaseResponse(1074, "Narrator not found.");
        } else {
            $status = 200;
            $dataResult = array("setup_narrator" => $item->toArray());
            $response = Utilities::buildSuccessResponse(1075, "Narrator data.", $dataResult);
        }

        return response()->json($response, $status);
    }

    /**
     * Fetch list of Role by searching with optional filters..
     *
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function getAllSetupNarrator(Request $request)
    {
        $mdl = new SetupNarrator();

        $this->validate($request, $mdl->rules($request, Constant::RequestType['GET_ALL']), $mdl->messages($request, Constant::RequestType['GET_ALL']));

        $pageSize = $request->limit ?? ProjectConstant::PageSize;
        if ($pageSize > ProjectConstant::MaxPageSize) {
            $pageSize = ProjectConstant::PageSize;
        }
        $page = $request->page ?? ProjectConstant::Page;
        $skip = ($page - 1) * $pageSize;


        $select = $this->select_columns;

        $mdl->filterColumns($request);

        if ($request->fields) {
            $select = $request->fields;
        }

        $whereData = array();
        if ($request->nratr_name) {
            $whereData[] = ['nratr_name', 'LIKE', "%{$request->nratr_name}%"];
        }
        if ($request->father_name) {
            $whereData[] = ['father_name', 'LIKE', "%{$request->father_name}%"];
        }
        if ($request->tabqa_id) {
            $whereData[] = ['tabqa_id', '=', "%{$request->tabqa_id}%"];
        }
        if ($request->kuniyat) {
            $whereData[] = ['kuniyat', 'LIKE', "%{$request->kuniyat}%"];
        }
        if ($request->nasab) {
            $whereData[] = ['nasab', 'LIKE', "%{$request->nasab}%"];
        }
        if ($request->laqab) {
            $whereData[] = ['laqab', 'LIKE', "%{$request->laqab}%"];
        }
        if ($request->nratr_dob) {
            $whereData[] = ['nratr_dob', '=', "%{$request->nratr_dob}%"];
        }
        if ($request->nratr_pob) {
            $whereData[] = ['nratr_pob', 'LIKE', "%{$request->nratr_pob}%"];
        }
        if ($request->nratr_pod) {
            $whereData[] = ['nratr_pod', 'LKE', "%{$request->nratr_pod}%"];
        }
        if ($request->nratr_teachers) {
            $whereData[] = ['nratr_teachers', 'LIKE', "%{$request->nratr_teachers}%"];
        }
        if ($request->nratr_student) {
            $whereData[] = ['nratr_student', 'LIKE', "%{$request->nratr_student}%"];
        }
        if ($request->activate != null) {
            $whereData[] = ['is_enable', $request->activate];
        }


        $itemCount = SetupNarrator::with('tabqa')->where($whereData)->active()->get()->count();

        $orderBy = $mdl->getOrderColumn($request->order_by);
        $orderType = $request->order_type ?? ProjectConstant::OrderType;

        $items = SetupNarrator::with('tabqa')->where($whereData)
            ->active()
            ->orderBy($orderBy, $orderType)
            ->offset($skip)
            ->limit($pageSize)
            ->get($select);


        $status = 200;
        $data_result = array("setup_narrators_total" => $itemCount, "setup_narrators" => $items->toArray());
        $response = Utilities::buildSuccessResponse(1076, "Narrator list.", $data_result);

        return response()->json($response, $status);
    }

}
