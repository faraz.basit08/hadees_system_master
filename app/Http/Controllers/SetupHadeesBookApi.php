<?php

namespace App\Http\Controllers;

use App\Config\ProjectConstant;
use App\Models\SetupHadeesBook;
use App\Models\UserRoles;
use App\Response\HadeesBookResponse;
use App\Response\UserRolesResponse;
use DiUtil\Config\Constant;
use DiUtil\Utilities\Utilities;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller;

class SetupHadeesBookApi extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    private $select_columns = ['id', 'book_name', 'is_enable as activate'];

    /**
     * This fucntion is called after validation fails in function $this->validate.
     *
     * @param Request $request
     * @param array $errors
     * @return \Illuminate\Http\JsonResponse
     */

    protected function buildFailedValidationResponse(Request $request, array $errors){
        $response = Utilities::buildFailedValidationResponse(10000, "Unprocesssable Entity.", $errors);
        return response()->json($response, 400);
    }
    /**
     * Add Hadees Book.
     *
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function addSetupHadeesBook(Request $request)
    {
        $mdlHadeesBook = new SetupHadeesBook();

        $this->validate($request, $mdlHadeesBook->rules($request), $mdlHadeesBook->messages($request));

        $mdlHadeesBook->filterColumns($request);

        Utilities::defaultAddAttributes($request);

        $obj = SetupHadeesBook::create($request->all());

        $data = ['id' => $obj->id];

        $response = Utilities::buildSuccessResponse(1070, "Hadees book successfully created.", $data);

        return response()->json($response, 201);
    }

    /**
     * Update Role.
     *
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateSetupHadeesBook(Request $request)
    {
        $mdlHadeesBook = new SetupHadeesBook();

        $this->validate($request, $mdlHadeesBook->rules($request), $mdlHadeesBook->messages($request));

        $mdlHadeesBook->filterColumns($request);

        Utilities::defaultUpdateAttributes($request);

        $role = SetupHadeesBook::find($request->id);

        $status = 200;
        $response = [];

        if (!$role) {
            $status = 404;
            $response = Utilities::buildBaseResponse(1074, "Hadees book not found.");
        } else {

            $obj = $role->update($request->all());

            if ($obj) {
                $data = ['id' => $role->id];
                $response = Utilities::buildSuccessResponse(1071, "Hadees book successfully updated.", $data);
            }
        }

        return response()->json($response, $status);
    }

    /**
     * Activate/De-Activate Role.
     *
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function patchDisableEnableSetupHadeesBook(Request $request)
    {
        $mdlHadeesBook = new SetupHadeesBook();

        Utilities::removeAttributesExcept($request, ["id", "activate"]);

        $this->validate($request, $mdlHadeesBook->rules($request), $mdlHadeesBook->messages($request));

        Utilities::defaultUpdateAttributes($request);

        $activate = $request->activate == 1 ? Constant::RecordType['ENABLED'] : Constant::RecordType['DISABLED'];

        $request->request->add(['is_enable' => $activate]);

        $hadeesBook = SetupHadeesBook::find($request->id);

        $status = 200;
        $response = [];

        if (!$hadeesBook) {
            $status = 404;
            $response = Utilities::buildBaseResponse(1074, "Hadees book not found.");
        } else {

            $obj = $hadeesBook->update($request->all());

            if ($obj) {
                $data = ['id' => $hadeesBook->id];
                $actMsg = $request->activate == 1 ? "activated" : "de-activated";
                $response = Utilities::buildSuccessResponse(1072, "Hadees book successfully $actMsg.", $data);
            }
        }

        return response()->json($response, $status);
    }

    /**
     * Delete Role.
     *
     * @param $id 'ID' of Role to delete. (required)
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteSetupHadeesBook($id, Request $request)
    {
        $mdlHadeesBook = new SetupHadeesBook();

        $request->request->add(['id' => $id]);

        $this->validate($request, $mdlHadeesBook->rules($request), $mdlHadeesBook->messages($request));

        Utilities::defaultDeleteAttributes($request);

        $request->request->add(['is_enable' => Constant::RecordType['DELETED']]);

        $hadeesBook = SetupHadeesBook::find($request->id);

        $status = 200;
        $response = [];
        if (!$hadeesBook) {
            $status = 404;
            $response = Utilities::buildBaseResponse(1074, "Hadees book not found.");
        } else {

            $obj = $hadeesBook->update($request->all());
            if ($obj) {
                $response = Utilities::buildBaseResponse(1073, "Hadees book successfully deleted.");
            }
        }

        return response()->json($response, $status);
    }

    /**
     * Get one Role.
     *
     * @param $id 'ID' of Role to return (required)
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getOneSetupHadeesBook($id, Request $request)
    {
        $mdlHadeesBook = new SetupHadeesBook();

        $request->request->add(['id' => $id]);

        $this->validate($request, $mdlHadeesBook->rules($request, Constant::RequestType['GET_ONE']), $mdlHadeesBook->messages($request, Constant::RequestType['GET_ONE']));

        $select = $this->select_columns;

        $mdlHadeesBook->filterColumns($request);

        if ($request->fields) {
            $select = $request->fields;
        }

        $hadeesBook = SetupHadeesBook::where('id', $request->id)->first($select);

        $status = 200;
        $response = [];

        if (!$hadeesBook) {
            $status = 404;
            $response = Utilities::buildBaseResponse(1074, "Hadees book not found.");
        } else {
            $status = 200;
            $dataResult = array("hadees_book" => $hadeesBook->toArray());
            $response = Utilities::buildSuccessResponse(1075, "Hadees book data.", $dataResult);
        }

        return response()->json($response, $status);
    }

    /**
     * Fetch list of Role by searching with optional filters..
     *
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllSetupHadeesBook(Request $request)
    {
        $mdlHadeesBook = new SetupHadeesBook();

        $this->validate($request, $mdlHadeesBook->rules($request, Constant::RequestType['GET_ALL']), $mdlHadeesBook->messages($request, Constant::RequestType['GET_ALL']));

        $pageSize = $request->limit ?? ProjectConstant::PageSize;
        if ($pageSize > ProjectConstant::MaxPageSize) {
            $pageSize = ProjectConstant::PageSize;
        }
        $page = $request->page ?? ProjectConstant::Page;
        $skip = ($page - 1) * $pageSize;


        $select = $this->select_columns;

        $mdlHadeesBook->filterColumns($request);

        if ($request->fields) {
            $select = $request->fields;
        }

        $whereData = array();
        if ($request->book_name) {
            $whereData[] = ['book_name', 'LIKE', "%{$request->book_name}%"];
        }
        if ($request->activate != null) {
            $whereData[] = ['is_enable', $request->activate];
        }

        $hadeesBooksCount = SetupHadeesBook::where($whereData)->active()->get()->count();

        $orderBy =  $mdlHadeesBook->getOrderColumn($request->order_by);
        $orderType = $request->order_type ?? ProjectConstant::OrderType;

        $hadeesBooks = SetupHadeesBook::where($whereData)
        ->active()
        ->orderBy($orderBy, $orderType)
        ->offset($skip)
        ->limit($pageSize)
        ->get($select);

        $status = 200;
        $data_result = array("hadees_book_total" => $hadeesBooksCount, "hadees_book" => $hadeesBooks->toArray());
        $response = Utilities::buildSuccessResponse(1076, "Hadees book list.", $data_result);

        return response()->json($response, $status);
    }

}
