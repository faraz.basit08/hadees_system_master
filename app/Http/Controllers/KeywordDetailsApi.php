<?php

namespace App\Http\Controllers;

use App\Config\ProjectConstant;
use App\Models\KeywordDetails;
use App\Models\UserRoles;
use App\Response\KeywordDetailsResponse;
use App\Response\UserRolesResponse;
use DiUtil\Config\Constant;
use DiUtil\Utilities\Utilities;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller;

class KeywordDetailsApi extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    private $select_columns = ['id', 'keyword_id','keyword_detail','language_id', 'is_enable as activate'];

    /**
     * This fucntion is called after validation fails in function $this->validate.
     *
     * @param Request $request
     * @param array $errors
     * @return \Illuminate\Http\JsonResponse
     */

    protected function buildFailedValidationResponse(Request $request, array $errors){
        $response = Utilities::buildFailedValidationResponse(10000, "Unprocesssable Entity.", $errors);
        return response()->json($response, 400);
    }
    /**
     * Add Role.
     *
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function addKeywordDetails(Request $request)
    {
        $mdlKeywordDetails = new KeywordDetails();

        $this->validate($request, $mdlKeywordDetails->rules($request), $mdlKeywordDetails->messages($request));

        $mdlKeywordDetails->filterColumns($request);

        Utilities::defaultAddAttributes($request);

        $obj = KeywordDetails::create($request->all());

        $data = ['id' => $obj->id];

        $response = Utilities::buildSuccessResponse(1070, "Keyword detail successfully created.", $data);

        return response()->json($response, 201);
    }

    /**
     * Update Role.
     *
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateKeywordDetails(Request $request)
    {
        $mdlKeywordDetails = new KeywordDetails();

        $this->validate($request, $mdlKeywordDetails->rules($request), $mdlKeywordDetails->messages($request));

        $mdlKeywordDetails->filterColumns($request);

        Utilities::defaultUpdateAttributes($request);

        $role = KeywordDetails::find($request->id);

        $status = 200;
        $response = [];

        if (!$role) {
            $status = 404;
            $response = Utilities::buildBaseResponse(1074, "Keyword detail not found.");
        } else {

            $obj = $role->update($request->all());

            if ($obj) {
                $data = ['id' => $role->id];
                $response = Utilities::buildSuccessResponse(1071, "Keyword detail successfully updated.", $data);
            }
        }

        return response()->json($response, $status);
    }

    /**
     * Activate/De-Activate Role.
     *
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function patchDisableEnableKeywordDetails(Request $request)
    {
        $mdlKeywordDetails = new KeywordDetails();

        Utilities::removeAttributesExcept($request, ["id", "activate"]);

        $this->validate($request, $mdlKeywordDetails->rules($request), $mdlKeywordDetails->messages($request));

        Utilities::defaultUpdateAttributes($request);

        $activate = $request->activate == 1 ? Constant::RecordType['ENABLED'] : Constant::RecordType['DISABLED'];

        $request->request->add(['is_enable' => $activate]);

        $language = KeywordDetails::find($request->id);

        $status = 200;
        $response = [];

        if (!$language) {
            $status = 404;
            $response = Utilities::buildBaseResponse(1074, "Keyword detail not found.");
        } else {

            $obj = $language->update($request->all());

            if ($obj) {
                $data = ['id' => $language->id];
                $actMsg = $request->activate == 1 ? "activated" : "de-activated";
                $response = Utilities::buildSuccessResponse(1072, "Keyword detail successfully $actMsg.", $data);
            }
        }

        return response()->json($response, $status);
    }

    /**
     * Delete Role.
     *
     * @param $id 'ID' of Role to delete. (required)
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteKeywordDetails($id, Request $request)
    {
        $mdlKeywordDetails = new KeywordDetails();

        $request->request->add(['id' => $id]);

        $this->validate($request, $mdlKeywordDetails->rules($request), $mdlKeywordDetails->messages($request));

        Utilities::defaultDeleteAttributes($request);

        $request->request->add(['is_enable' => Constant::RecordType['DELETED']]);

        $language = KeywordDetails::find($request->id);

        $status = 200;
        $response = [];
        if (!$language) {
            $status = 404;
            $response = Utilities::buildBaseResponse(1074, "Keyword detail not found.");
        } else {

            $obj = $language->update($request->all());
            if ($obj) {
                $response = Utilities::buildBaseResponse(1073, "Keyword detail successfully deleted.");
            }
        }

        return response()->json($response, $status);
    }

    /**
     * Get one Role.
     *
     * @param $id 'ID' of Role to return (required)
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getOneKeywordDetails($id, Request $request)
    {
        $mdlKeywordDetails = new KeywordDetails();

        $request->request->add(['id' => $id]);

        $this->validate($request, $mdlKeywordDetails->rules($request, Constant::RequestType['GET_ONE']), $mdlKeywordDetails->messages($request, Constant::RequestType['GET_ONE']));

        $select = $this->select_columns;

        $mdlKeywordDetails->filterColumns($request);

        if ($request->fields) {
            $select = $request->fields;
        }

        $language = KeywordDetails::with('language','keyword')->where('id', $request->id)->first($select);

        $status = 200;
        $response = [];

        if (!$language) {
            $status = 404;
            $response = Utilities::buildBaseResponse(1074, "Keyword detail not found.");
        } else {
            $status = 200;
            $dataResult = array("keyword_detail" => $language->toArray());
            $response = Utilities::buildSuccessResponse(1075, "Keyword detail data.", $dataResult);
        }

        return response()->json($response, $status);
    }

    /**
     * Fetch list of Role by searching with optional filters..
     *
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllKeywordDetails(Request $request)
    {
        $mdlKeywordDetails = new KeywordDetails();

        $this->validate($request, $mdlKeywordDetails->rules($request, Constant::RequestType['GET_ALL']), $mdlKeywordDetails->messages($request, Constant::RequestType['GET_ALL']));

        $pageSize = $request->limit ?? ProjectConstant::PageSize;
        if ($pageSize > ProjectConstant::MaxPageSize) {
            $pageSize = ProjectConstant::PageSize;
        }
        $page = $request->page ?? ProjectConstant::Page;
        $skip = ($page - 1) * $pageSize;


        $select = $this->select_columns;

        $mdlKeywordDetails->filterColumns($request);

        if ($request->fields) {
            $select = $request->fields;
        }

        $whereData = array();
        if ($request->language_name) {
            $whereData[] = ['language_name', 'LIKE', "%{$request->language_name}%"];
        }
        if ($request->activate != null) {
            $whereData[] = ['is_enable', $request->activate];
        }

        $languageCount = KeywordDetails::with('language','keyword')->where($whereData)->active()->get()->count();

        $orderBy =  $mdlKeywordDetails->getOrderColumn($request->order_by);
        $orderType = $request->order_type ?? ProjectConstant::OrderType;

        $language = KeywordDetails::with('language','keyword')->where($whereData)
        ->active()
        ->orderBy($orderBy, $orderType)
        ->offset($skip)
        ->limit($pageSize)
        ->get($select);

        $status = 200;
        $data_result = array("keyword_detail_total" => $languageCount, "keyword_detail" => $language->toArray());
        $response = Utilities::buildSuccessResponse(1076, "Keyword detail list.", $data_result);

        return response()->json($response, $status);
    }

}
