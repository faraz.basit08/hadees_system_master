<?php

namespace App\Http\Controllers;

use App\Config\ProjectConstant;
use App\Models\HadeesVolume;
use App\Models\UserRoles;
use App\Response\HadeesVolumeResponse;
use App\Response\UserRolesResponse;
use DiUtil\Config\Constant;
use DiUtil\Utilities\Utilities;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller;

class HadeesVolumeApi extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    private $select_columns = ['id', 'vol_number','hds_book_id', 'is_enable as activate'];

    /**
     * This fucntion is called after validation fails in function $this->validate.
     *
     * @param Request $request
     * @param array $errors
     * @return \Illuminate\Http\JsonResponse
     */

    protected function buildFailedValidationResponse(Request $request, array $errors){
        $response = Utilities::buildFailedValidationResponse(10000, "Unprocesssable Entity.", $errors);
        return response()->json($response, 400);
    }
    /**
     * Add Hadees Volume.
     *
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function addHadeesVolume(Request $request)
    {

        $mdlHadeesVolume = new HadeesVolume();

        $this->validate($request, $mdlHadeesVolume->rules($request), $mdlHadeesVolume->messages($request));

        $mdlHadeesVolume->filterColumns($request);

        Utilities::defaultAddAttributes($request);

        $obj = HadeesVolume::create($request->all());

        $data = ['id' => $obj->id];

        $response = Utilities::buildSuccessResponse(1070, "Hadees volume successfully created.", $data);

        return response()->json($response, 201);
    }

    /**
     * Update Role.
     *
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateHadeesVolume(Request $request)
    {
        $mdlHadeesVolume = new HadeesVolume();

        $this->validate($request, $mdlHadeesVolume->rules($request), $mdlHadeesVolume->messages($request));

        $mdlHadeesVolume->filterColumns($request);

        Utilities::defaultUpdateAttributes($request);

        $role = HadeesVolume::find($request->id);

        $status = 200;
        $response = [];

        if (!$role) {
            $status = 404;
            $response = Utilities::buildBaseResponse(1074, "Hadees volume not found.");
        } else {

            $obj = $role->update($request->all());

            if ($obj) {
                $data = ['id' => $role->id];
                $response = Utilities::buildSuccessResponse(1071, "Hadees volume successfully updated.", $data);
            }
        }

        return response()->json($response, $status);
    }

    /**
     * Activate/De-Activate Role.
     *
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function patchDisableEnableHadeesVolume(Request $request)
    {
        $mdlHadeesVolume = new HadeesVolume();

        Utilities::removeAttributesExcept($request, ["id", "activate"]);

        $this->validate($request, $mdlHadeesVolume->rules($request), $mdlHadeesVolume->messages($request));

        Utilities::defaultUpdateAttributes($request);

        $activate = $request->activate == 1 ? Constant::RecordType['ENABLED'] : Constant::RecordType['DISABLED'];

        $request->request->add(['is_enable' => $activate]);

        $hadeesVolume = HadeesVolume::find($request->id);

        $status = 200;
        $response = [];

        if (!$hadeesVolume) {
            $status = 404;
            $response = Utilities::buildBaseResponse(1074, "Hadees volume not found.");
        } else {

            $obj = $hadeesVolume->update($request->all());

            if ($obj) {
                $data = ['id' => $hadeesVolume->id];
                $actMsg = $request->activate == 1 ? "activated" : "de-activated";
                $response = Utilities::buildSuccessResponse(1072, "Hadees volume successfully $actMsg.", $data);
            }
        }

        return response()->json($response, $status);
    }

    /**
     * Delete Role.
     *
     * @param $id 'ID' of Role to delete. (required)
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteHadeesVolume($id, Request $request)
    {
        $mdlHadeesVolume = new HadeesVolume();

        $request->request->add(['id' => $id]);

        $this->validate($request, $mdlHadeesVolume->rules($request), $mdlHadeesVolume->messages($request));

        Utilities::defaultDeleteAttributes($request);

        $request->request->add(['is_enable' => Constant::RecordType['DELETED']]);

        $hadeesVolume = HadeesVolume::find($request->id);

        $status = 200;
        $response = [];
        if (!$hadeesVolume) {
            $status = 404;
            $response = Utilities::buildBaseResponse(1074, "Hadees volume not found.");
        } else {

            $obj = $hadeesVolume->update($request->all());
            if ($obj) {
                $response = Utilities::buildBaseResponse(1073, "Hadees volume successfully deleted.");
            }
        }

        return response()->json($response, $status);
    }

    /**
     * Get one Role.
     *
     * @param $id 'ID' of Role to return (required)
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getOneHadeesVolume($id, Request $request)
    {
        $mdlHadeesVolume = new HadeesVolume();

        $request->request->add(['id' => $id]);

        $this->validate($request, $mdlHadeesVolume->rules($request, Constant::RequestType['GET_ONE']), $mdlHadeesVolume->messages($request, Constant::RequestType['GET_ONE']));

        $select = $this->select_columns;

        $mdlHadeesVolume->filterColumns($request);

        if ($request->fields) {
            $select = $request->fields;
        }

        $hadeesVolume = HadeesVolume::where('id', $request->id)->first($select);

        $status = 200;
        $response = [];

        if (!$hadeesVolume) {
            $status = 404;
            $response = Utilities::buildBaseResponse(1074, "Hadees volume not found.");
        } else {
            $status = 200;
            $dataResult = array("hadees_volume" => $hadeesVolume->toArray());
            $response = Utilities::buildSuccessResponse(1075, "Hadees volume data.", $dataResult);
        }

        return response()->json($response, $status);
    }

    /**
     * Fetch list of Role by searching with optional filters..
     *
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllHadeesVolume(Request $request)
    {
        $mdlHadeesVolume = new HadeesVolume();

        $this->validate($request, $mdlHadeesVolume->rules($request, Constant::RequestType['GET_ALL']), $mdlHadeesVolume->messages($request, Constant::RequestType['GET_ALL']));

        $pageSize = $request->limit ?? ProjectConstant::PageSize;
        if ($pageSize > ProjectConstant::MaxPageSize) {
            $pageSize = ProjectConstant::PageSize;
        }
        $page = $request->page ?? ProjectConstant::Page;
        $skip = ($page - 1) * $pageSize;


        $select = $this->select_columns;

        $mdlHadeesVolume->filterColumns($request);

        if ($request->fields) {
            $select = $request->fields;
        }

        $whereData = array();
        if ($request->hds_book_id) {
            $whereData[] = ['hds_book_id', '=', $request->hds_book_id];
        }
        if ($request->vol_number) {
            $whereData[] = ['vol_number', 'LIKE', "%{$request->vol_number}%"];
        }
        if ($request->activate != null) {
            $whereData[] = ['is_enable', $request->activate];
        }

        $hadeesVolumeCount = HadeesVolume::with('hadees_book')->where($whereData)->active()->get()->count();

        $orderBy =  $mdlHadeesVolume->getOrderColumn($request->order_by);
        $orderType = $request->order_type ?? ProjectConstant::OrderType;

        $hadeesVolume = HadeesVolume::with('hadees_book')->where($whereData)
        ->active()
        ->orderBy($orderBy, $orderType)
        ->offset($skip)
        ->limit($pageSize)
        ->get($select);


        $status = 200;
        $data_result = array("hadees_volume_total" => $hadeesVolumeCount, "hadees_volume" => $hadeesVolume->toArray());
        $response = Utilities::buildSuccessResponse(1076, "Hadees volume list.", $data_result);

        return response()->json($response, $status);
    }

}
