<?php

namespace App\Http\Controllers;

use App\Config\ProjectConstant;
use App\Models\SetupSharah;
use DiUtil\Config\Constant;
use DiUtil\Utilities\Utilities;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller;

class SetupSharahAPI extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    private $select_columns = [
        'id',
        'sharah_name',
        'is_enable as activate',
    ];

    /**
     * This fucntion is called after validation fails in function $this->validate.
     *
     * @param Request $request
     * @param array $errors
     * @return \Illuminate\Http\JsonResponse
     */

    protected function buildFailedValidationResponse(Request $request, array $errors)
    {
        $response = Utilities::buildFailedValidationResponse(10000, "Unprocesssable Entity.", $errors);
        return response()->json($response, 400);
    }

    /**
     * Add Role.
     *
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function addSetupSharah(Request $request)
    {

        $mdl = new SetupSharah();

        $this->validate($request, $mdl->rules($request), $mdl->messages($request));
        $mdl->filterColumns($request);
        Utilities::defaultAddAttributes($request);

        $item = SetupSharah::create($request->all());

        $data = ['id' => $item->id];

        $response = Utilities::buildSuccessResponse(1070, "Sharah successfully created.", $data);

        return response()->json($response, 201);
    }

    /**
     * Update Role.
     *
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function updateSetupSharah(Request $request)
    {
        $mdl = new SetupSharah();

        $this->validate($request, $mdl->rules($request), $mdl->messages($request));

        $mdl->filterColumns($request);

        Utilities::defaultUpdateAttributes($request);

        $item = SetupSharah::find($request->id);


        if (!$item) {
            $status = 404;
            $response = Utilities::buildBaseResponse(1074, "Sharah not found.");
        } else {
            $status = 200;

            $obj = $item->update($request->all());

            if ($obj) {
                $data = ['id' => $item->id];
                $response = Utilities::buildSuccessResponse(1071, "Sharah successfully updated.", $data);
            }
        }

        return response()->json($response, $status);
    }

    /**
     * Activate/De-Activate Role.
     *
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function patchDisableEnableSetupSharah(Request $request)
    {
        $mdl = new SetupSharah();

        Utilities::removeAttributesExcept($request, ["id", "activate"]);

        $this->validate($request, $mdl->rules($request), $mdl->messages($request));

        Utilities::defaultUpdateAttributes($request);

        $activate = $request->activate == 1 ? Constant::RecordType['ENABLED'] : Constant::RecordType['DISABLED'];

        $request->request->add(['is_enable' => $activate]);

        $item = SetupSharah::find($request->id);

        $response = [];

        if (!$item) {
            $status = 404;
            $response = Utilities::buildBaseResponse(1074, "Sharah not found.");
        } else {
            $status = 200;

            $obj = $item->update($request->all());

            if ($obj) {
                $data = ['id' => $item->id];
                $actMsg = $request->activate == 1 ? "activated" : "de-activated";
                $response = Utilities::buildSuccessResponse(1072, "Sharah successfully $actMsg.", $data);
            }
        }

        return response()->json($response, $status);
    }

    /**
     * Delete Role.
     *
     * @param $id 'ID' of Role to delete. (required)
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function deleteSetupSharah($id, Request $request)
    {
        $mdl = new SetupSharah();

        $request->request->add(['id' => $id]);

        $this->validate($request, $mdl->rules($request), $mdl->messages($request));

        Utilities::defaultDeleteAttributes($request);

        $request->request->add(['is_enable' => Constant::RecordType['DELETED']]);

        $item = SetupSharah::find($request->id);

        if (!$item) {
            $status = 404;
            $response = Utilities::buildBaseResponse(1074, "Sharah not found.");
        } else {
            $status = 200;

            $obj = $item->update($request->all());

            if ($obj) {
                $response = Utilities::buildBaseResponse(1073, "Sharah successfully deleted.");
            }
        }

        return response()->json($response, $status);
    }

    /**
     * Get one Role.
     *
     * @param $id 'ID' of Role to return (required)
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function getOneSetupSharah($id, Request $request)
    {
        $mdl = new SetupSharah();

        $request->request->add(['id' => $id]);

        $this->validate($request, $mdl->rules($request, Constant::RequestType['GET_ONE']), $mdl->messages($request, Constant::RequestType['GET_ONE']));

        $select = $this->select_columns;

        $mdl->filterColumns($request);

        if ($request->fields) {
            $select = $request->fields;
        }

        $item = SetupSharah::where('id', $request->id)->first($select);

        if (!$item) {
            $status = 404;
            $response = Utilities::buildBaseResponse(1074, "Sharah not found.");
        } else {
            $status = 200;
            $dataResult = array("setup_sharah" => $item->toArray());
            $response = Utilities::buildSuccessResponse(1075, "Sharah data.", $dataResult);
        }

        return response()->json($response, $status);
    }

    /**
     * Fetch list of Role by searching with optional filters..
     *
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function getAllSetupSharah(Request $request)
    {
        $mdl = new SetupSharah();

        $this->validate($request, $mdl->rules($request, Constant::RequestType['GET_ALL']), $mdl->messages($request, Constant::RequestType['GET_ALL']));

        $pageSize = $request->limit ?? ProjectConstant::PageSize;
        if ($pageSize > ProjectConstant::MaxPageSize) {
            $pageSize = ProjectConstant::PageSize;
        }
        $page = $request->page ?? ProjectConstant::Page;
        $skip = ($page - 1) * $pageSize;


        $select = $this->select_columns;

        $mdl->filterColumns($request);

        if ($request->fields) {
            $select = $request->fields;
        }

        $whereData = array();
        if ($request->sharah_name) {
            $whereData[] = ['sharah_name', 'LIKE', "%{$request->sharah_name}%"];
        }
        if ($request->activate != null) {
            $whereData[] = ['is_enable', $request->activate];
        }

        $itemCount = SetupSharah::where($whereData)->active()->get()->count();

        $orderBy = $mdl->getOrderColumn($request->order_by);
        $orderType = $request->order_type ?? ProjectConstant::OrderType;

        $items = SetupSharah::where($whereData)
            ->active()
            ->orderBy($orderBy, $orderType)
            ->offset($skip)
            ->limit($pageSize)
            ->get($select);

        $status = 200;
        $data_result = array("setup_sharahs_total" => $itemCount, "setup_sharahs" => $items->toArray());
        $response = Utilities::buildSuccessResponse(1076, "Sharah list.", $data_result);

        return response()->json($response, $status);
    }

}
