<?php

namespace App\Http\Controllers;

use App\Config\ProjectConstant;
use App\Models\HadeesMuhaddisMapping;
use App\Models\UserRoles;
use App\Response\UserRolesResponse;
use DiUtil\Config\Constant;
use DiUtil\Utilities\Utilities;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller;

class HadeesMuhaddisMappingApi extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    private $select_columns = ['id', 'muhaddis_id','hadees_book_id','muhaddis_intro','dob','dou', 'is_enable as activate'];

    /**
     * This fucntion is called after validation fails in function $this->validate.
     *
     * @param Request $request
     * @param array $errors
     * @return \Illuminate\Http\JsonResponse
     */

    protected function buildFailedValidationResponse(Request $request, array $errors){
        $response = Utilities::buildFailedValidationResponse(10000, "Unprocesssable Entity.", $errors);
        return response()->json($response, 400);
    }
    /**
     * Add Role.
     *
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function addHadeesMuhaddisMapping(Request $request)
    {
        $mdlHadeesMuhaddisMapping = new HadeesMuhaddisMapping();

        $this->validate($request, $mdlHadeesMuhaddisMapping->rules($request), $mdlHadeesMuhaddisMapping->messages($request));

        $mdlHadeesMuhaddisMapping->filterColumns($request);

        Utilities::defaultAddAttributes($request);

        $obj = HadeesMuhaddisMapping::create($request->all());

        $data = ['id' => $obj->id];

        $response = Utilities::buildSuccessResponse(1070, "Hadees SetupMuhaddis Mapping successfully created.", $data);

        return response()->json($response, 201);
    }

    /**
     * Update Role.
     *
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateHadeesMuhaddisMapping(Request $request)
    {
        $mdlHadeesMuhaddisMapping = new HadeesMuhaddisMapping();

        $this->validate($request, $mdlHadeesMuhaddisMapping->rules($request), $mdlHadeesMuhaddisMapping->messages($request));

        $mdlHadeesMuhaddisMapping->filterColumns($request);

        Utilities::defaultUpdateAttributes($request);

        $role = HadeesMuhaddisMapping::find($request->id);

        $status = 200;
        $response = [];

        if (!$role) {
            $status = 404;
            $response = Utilities::buildBaseResponse(1074, "Hadees SetupMuhaddis Mapping not found.");
        } else {

            $obj = $role->update($request->all());

            if ($obj) {
                $data = ['id' => $role->id];
                $response = Utilities::buildSuccessResponse(1071, "Hadees SetupMuhaddis Mapping successfully updated.", $data);
            }
        }

        return response()->json($response, $status);
    }

    /**
     * Activate/De-Activate Role.
     *
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function patchDisableEnableHadeesMuhaddisMapping(Request $request)
    {
        $mdlHadeesMuhaddisMapping = new HadeesMuhaddisMapping();

        Utilities::removeAttributesExcept($request, ["id", "activate"]);

        $this->validate($request, $mdlHadeesMuhaddisMapping->rules($request), $mdlHadeesMuhaddisMapping->messages($request));

        Utilities::defaultUpdateAttributes($request);

        $activate = $request->activate == 1 ? Constant::RecordType['ENABLED'] : Constant::RecordType['DISABLED'];

        $request->request->add(['is_enable' => $activate]);

        $hadeesBook = HadeesMuhaddisMapping::find($request->id);

        $status = 200;
        $response = [];

        if (!$hadeesBook) {
            $status = 404;
            $response = Utilities::buildBaseResponse(1074, "Hadees SetupMuhaddis Mapping not found.");
        } else {

            $obj = $hadeesBook->update($request->all());

            if ($obj) {
                $data = ['id' => $hadeesBook->id];
                $actMsg = $request->activate == 1 ? "activated" : "de-activated";
                $response = Utilities::buildSuccessResponse(1072, "Hadees SetupMuhaddis Mapping successfully $actMsg.", $data);
            }
        }

        return response()->json($response, $status);
    }

    /**
     * Delete Role.
     *
     * @param $id 'ID' of Role to delete. (required)
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteHadeesMuhaddisMapping($id, Request $request)
    {
        $mdlHadeesMuhaddisMapping = new HadeesMuhaddisMapping();

        $request->request->add(['id' => $id]);

        $this->validate($request, $mdlHadeesMuhaddisMapping->rules($request), $mdlHadeesMuhaddisMapping->messages($request));

        Utilities::defaultDeleteAttributes($request);

        $request->request->add(['is_enable' => Constant::RecordType['DELETED']]);

        $hadeesBook = HadeesMuhaddisMapping::find($request->id);

        $status = 200;
        $response = [];
        if (!$hadeesBook) {
            $status = 404;
            $response = Utilities::buildBaseResponse(1074, "Hadees SetupMuhaddis Mapping not found.");
        } else {

            $obj = $hadeesBook->update($request->all());
            if ($obj) {
                $response = Utilities::buildBaseResponse(1073, "Hadees SetupMuhaddis Mapping successfully deleted.");
            }
        }

        return response()->json($response, $status);
    }

    /**
     * Get one Role.
     *
     * @param $id 'ID' of Role to return (required)
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getOneHadeesMuhaddisMapping($id, Request $request)
    {
        $mdlHadeesMuhaddisMapping = new HadeesMuhaddisMapping();

        $request->request->add(['id' => $id]);

        $this->validate($request, $mdlHadeesMuhaddisMapping->rules($request, Constant::RequestType['GET_ONE']), $mdlHadeesMuhaddisMapping->messages($request, Constant::RequestType['GET_ONE']));

        $select = $this->select_columns;

        $mdlHadeesMuhaddisMapping->filterColumns($request);

        if ($request->fields) {
            $select = $request->fields;
        }

        $hadeesBook = HadeesMuhaddisMapping::where('id', $request->id)->first($select);

        $status = 200;
        $response = [];

        if (!$hadeesBook) {
            $status = 404;
            $response = Utilities::buildBaseResponse(1074, "Hadees SetupMuhaddis Mapping not found.");
        } else {
            $status = 200;
            $dataResult = array("hadees_book" => $hadeesBook->toArray());
            $response = Utilities::buildSuccessResponse(1075, "Hadees SetupMuhaddis Mapping data.", $dataResult);
        }

        return response()->json($response, $status);
    }

    /**
     * Fetch list of Role by searching with optional filters..
     *
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllHadeesMuhaddisMapping(Request $request)
    {
        $mdlHadeesMuhaddisMapping = new HadeesMuhaddisMapping();

        $this->validate($request, $mdlHadeesMuhaddisMapping->rules($request, Constant::RequestType['GET_ALL']), $mdlHadeesMuhaddisMapping->messages($request, Constant::RequestType['GET_ALL']));

        $pageSize = $request->limit ?? ProjectConstant::PageSize;
        if ($pageSize > ProjectConstant::MaxPageSize) {
            $pageSize = ProjectConstant::PageSize;
        }
        $page = $request->page ?? ProjectConstant::Page;
        $skip = ($page - 1) * $pageSize;


        $select = $this->select_columns;

        $mdlHadeesMuhaddisMapping->filterColumns($request);

        if ($request->fields) {
            $select = $request->fields;
        }

        $whereData = array();
        if ($request->book_name) {
            $whereData[] = ['book_name', 'LIKE', "%{$request->book_name}%"];
        }
        if ($request->activate != null) {
            $whereData[] = ['is_enable', $request->activate];
        }

        $hadeesBooksCount = HadeesMuhaddisMapping::where($whereData)->active()->get()->count();

        $orderBy =  $mdlHadeesMuhaddisMapping->getOrderColumn($request->order_by);
        $orderType = $request->order_type ?? ProjectConstant::OrderType;

        $hadeesBooks = HadeesMuhaddisMapping::where($whereData)
        ->active()
        ->orderBy($orderBy, $orderType)
        ->offset($skip)
        ->limit($pageSize)
        ->get($select);

        $status = 200;
        $data_result = array("hadees_book_total" => $hadeesBooksCount, "hadees_book" => $hadeesBooks->toArray());
        $response = Utilities::buildSuccessResponse(1076, "Hadees SetupMuhaddis Mapping list.", $data_result);

        return response()->json($response, $status);
    }

}
