<?php

namespace App\Http\Controllers;

use App\Config\ProjectConstant;
use App\Models\SetupKeyword;
use App\Models\UserRoles;
use App\Response\SetupKeywordResponse;
use App\Response\UserRolesResponse;
use DiUtil\Config\Constant;
use DiUtil\Utilities\Utilities;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller;

class SetupKeywordApi extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    private $select_columns = ['id', 'keyword_name','language_id', 'is_enable as activate'];

    /**
     * This fucntion is called after validation fails in function $this->validate.
     *
     * @param Request $request
     * @param array $errors
     * @return \Illuminate\Http\JsonResponse
     */

    protected function buildFailedValidationResponse(Request $request, array $errors){
        $response = Utilities::buildFailedValidationResponse(10000, "Unprocesssable Entity.", $errors);
        return response()->json($response, 400);
    }
    /**
     * Add Role.
     *
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function addSetupKeyword(Request $request)
    {
        $mdlSetupKeyword = new SetupKeyword();

        $this->validate($request, $mdlSetupKeyword->rules($request), $mdlSetupKeyword->messages($request));

        $mdlSetupKeyword->filterColumns($request);

        Utilities::defaultAddAttributes($request);

        $obj = SetupKeyword::create($request->all());

        $data = ['id' => $obj->id];

        $response = Utilities::buildSuccessResponse(1070, "Keyword successfully created.", $data);

        return response()->json($response, 201);
    }

    /**
     * Update Role.
     *
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateSetupKeyword(Request $request)
    {
        $mdlSetupKeyword = new SetupKeyword();

        $this->validate($request, $mdlSetupKeyword->rules($request), $mdlSetupKeyword->messages($request));

        $mdlSetupKeyword->filterColumns($request);

        Utilities::defaultUpdateAttributes($request);

        $role = SetupKeyword::find($request->id);

        $status = 200;
        $response = [];

        if (!$role) {
            $status = 404;
            $response = Utilities::buildBaseResponse(1074, "Keyword not found.");
        } else {

            $obj = $role->update($request->all());

            if ($obj) {
                $data = ['id' => $role->id];
                $response = Utilities::buildSuccessResponse(1071, "Keyword successfully updated.", $data);
            }
        }

        return response()->json($response, $status);
    }

    /**
     * Activate/De-Activate Role.
     *
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function patchDisableEnableSetupKeyword(Request $request)
    {
        $mdlSetupKeyword = new SetupKeyword();

        Utilities::removeAttributesExcept($request, ["id", "activate"]);

        $this->validate($request, $mdlSetupKeyword->rules($request), $mdlSetupKeyword->messages($request));

        Utilities::defaultUpdateAttributes($request);

        $activate = $request->activate == 1 ? Constant::RecordType['ENABLED'] : Constant::RecordType['DISABLED'];

        $request->request->add(['is_enable' => $activate]);

        $language = SetupKeyword::find($request->id);

        $status = 200;
        $response = [];

        if (!$language) {
            $status = 404;
            $response = Utilities::buildBaseResponse(1074, "Keyword not found.");
        } else {

            $obj = $language->update($request->all());

            if ($obj) {
                $data = ['id' => $language->id];
                $actMsg = $request->activate == 1 ? "activated" : "de-activated";
                $response = Utilities::buildSuccessResponse(1072, "Keyword successfully $actMsg.", $data);
            }
        }

        return response()->json($response, $status);
    }

    /**
     * Delete Role.
     *
     * @param $id 'ID' of Role to delete. (required)
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteSetupKeyword($id, Request $request)
    {
        $mdlSetupKeyword = new SetupKeyword();

        $request->request->add(['id' => $id]);

        $this->validate($request, $mdlSetupKeyword->rules($request), $mdlSetupKeyword->messages($request));

        Utilities::defaultDeleteAttributes($request);

        $request->request->add(['is_enable' => Constant::RecordType['DELETED']]);

        $language = SetupKeyword::find($request->id);

        $status = 200;
        $response = [];
        if (!$language) {
            $status = 404;
            $response = Utilities::buildBaseResponse(1074, "Keyword not found.");
        } else {

            $obj = $language->update($request->all());
            if ($obj) {
                $response = Utilities::buildBaseResponse(1073, "Keyword successfully deleted.");
            }
        }

        return response()->json($response, $status);
    }

    /**
     * Get one Role.
     *
     * @param $id 'ID' of Role to return (required)
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getOneSetupKeyword($id, Request $request)
    {
        $mdlSetupKeyword = new SetupKeyword();

        $request->request->add(['id' => $id]);

        $this->validate($request, $mdlSetupKeyword->rules($request, Constant::RequestType['GET_ONE']), $mdlSetupKeyword->messages($request, Constant::RequestType['GET_ONE']));

        $select = $this->select_columns;

        $mdlSetupKeyword->filterColumns($request);

        if ($request->fields) {
            $select = $request->fields;
        }

        $keyword = SetupKeyword::with('language')->where('id', $request->id)->first($select);

        $status = 200;
        $response = [];

        if (!$keyword) {
            $status = 404;
            $response = Utilities::buildBaseResponse(1074, "Keyword not found.");
        } else {
            $status = 200;
            $dataResult = array("keyword" => $keyword->toArray());
            $response = Utilities::buildSuccessResponse(1075, "Keyword data.", $dataResult);
        }

        return response()->json($response, $status);
    }

    /**
     * Fetch list of Role by searching with optional filters..
     *
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllSetupKeyword(Request $request)
    {
        $mdlSetupKeyword = new SetupKeyword();

        $this->validate($request, $mdlSetupKeyword->rules($request, Constant::RequestType['GET_ALL']), $mdlSetupKeyword->messages($request, Constant::RequestType['GET_ALL']));

        $pageSize = $request->limit ?? ProjectConstant::PageSize;
        if ($pageSize > ProjectConstant::MaxPageSize) {
            $pageSize = ProjectConstant::PageSize;
        }
        $page = $request->page ?? ProjectConstant::Page;
        $skip = ($page - 1) * $pageSize;


        $select = $this->select_columns;

        $mdlSetupKeyword->filterColumns($request);

        if ($request->fields) {
            $select = $request->fields;
        }

        $whereData = array();
        if ($request->keyword_name) {
            $whereData[] = ['keyword_name', 'LIKE', "%{$request->keyword_name}%"];
        }

        if ($request->language_id) {
            $whereData[] = ['language_id', '=', $request->language_id];
        }
        if ($request->activate != null) {
            $whereData[] = ['is_enable', $request->activate];
        }

        $languageCount = SetupKeyword::with('language')->where($whereData)->active()->get()->count();

        $orderBy =  $mdlSetupKeyword->getOrderColumn($request->order_by);
        $orderType = $request->order_type ?? ProjectConstant::OrderType;

        $language = SetupKeyword::with('language')->where($whereData)
        ->active()
        ->orderBy($orderBy, $orderType)
        ->offset($skip)
        ->limit($pageSize)
        ->get($select);

        $status = 200;
        $data_result = array("keyword_total" => $languageCount, "keyword" => $language->toArray());
        $response = Utilities::buildSuccessResponse(1076, "Keyword list.", $data_result);

        return response()->json($response, $status);
    }

}
