<?php

namespace App\Http\Controllers;

use App\Config\ProjectConstant;
use App\Models\SetupHadeesSubBook;
use App\Models\UserRoles;
use App\Response\UserRolesResponse;
use DiUtil\Config\Constant;
use DiUtil\Utilities\Utilities;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller;

class SetupHadeesSubBookApi extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    private $select_columns = ['id', 'sub_book_name', 'hds_book_id','volume_id', 'is_enable as activate'];

    /**
     * This fucntion is called after validation fails in function $this->validate.
     *
     * @param Request $request
     * @param array $errors
     * @return \Illuminate\Http\JsonResponse
     */

    protected function buildFailedValidationResponse(Request $request, array $errors){
        $response = Utilities::buildFailedValidationResponse(10000, "Unprocesssable Entity.", $errors);
        return response()->json($response, 400);
    }
    /**
     * Add Role.
     *
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function addSetupHadeesSubBook(Request $request)
    {
        $mdlHadeesSubBook = new SetupHadeesSubBook();

        $this->validate($request, $mdlHadeesSubBook->rules($request), $mdlHadeesSubBook->messages($request));

        $mdlHadeesSubBook->filterColumns($request);

        Utilities::defaultAddAttributes($request);

        $obj = SetupHadeesSubBook::create($request->all());

        $data = ['id' => $obj->id];

        $response = Utilities::buildSuccessResponse(1070, "Hadees sub book successfully created.", $data);

        return response()->json($response, 201);
    }

    /**
     * Update Role.
     *
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateSetupHadeesSubBook(Request $request)
    {
        $mdlHadeesSubBook = new SetupHadeesSubBook();

        $this->validate($request, $mdlHadeesSubBook->rules($request), $mdlHadeesSubBook->messages($request));

        $mdlHadeesSubBook->filterColumns($request);

        Utilities::defaultUpdateAttributes($request);

        $role = SetupHadeesSubBook::find($request->id);

        $status = 200;
        $response = [];

        if (!$role) {
            $status = 404;
            $response = Utilities::buildBaseResponse(1074, "Hadees sub book not found.");
        } else {

            $obj = $role->update($request->all());

            if ($obj) {
                $data = ['id' => $role->id];
                $response = Utilities::buildSuccessResponse(1071, "Hadees sub book successfully updated.", $data);
            }
        }

        return response()->json($response, $status);
    }

    /**
     * Activate/De-Activate Role.
     *
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function patchDisableEnableSetupHadeesSubBook(Request $request)
    {
        $mdlHadeesSubBook = new SetupHadeesSubBook();

        Utilities::removeAttributesExcept($request, ["id", "activate"]);

        $this->validate($request, $mdlHadeesSubBook->rules($request), $mdlHadeesSubBook->messages($request));

        Utilities::defaultUpdateAttributes($request);

        $activate = $request->activate == 1 ? Constant::RecordType['ENABLED'] : Constant::RecordType['DISABLED'];

        $request->request->add(['is_enable' => $activate]);

        $hadeesBook = SetupHadeesSubBook::find($request->id);

        $status = 200;
        $response = [];

        if (!$hadeesBook) {
            $status = 404;
            $response = Utilities::buildBaseResponse(1074, "Hadees sub book not found.");
        } else {

            $obj = $hadeesBook->update($request->all());

            if ($obj) {
                $data = ['id' => $hadeesBook->id];
                $actMsg = $request->activate == 1 ? "activated" : "de-activated";
                $response = Utilities::buildSuccessResponse(1072, "Hadees sub book successfully $actMsg.", $data);
            }
        }

        return response()->json($response, $status);
    }

    /**
     * Delete Role.
     *
     * @param $id 'ID' of Role to delete. (required)
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteSetupHadeesSubBook($id, Request $request)
    {
        $mdlHadeesSubBook = new SetupHadeesSubBook();

        $request->request->add(['id' => $id]);

        $this->validate($request, $mdlHadeesSubBook->rules($request), $mdlHadeesSubBook->messages($request));

        Utilities::defaultDeleteAttributes($request);

        $request->request->add(['is_enable' => Constant::RecordType['DELETED']]);

        $hadeesBook = SetupHadeesSubBook::find($request->id);

        $status = 200;
        $response = [];
        if (!$hadeesBook) {
            $status = 404;
            $response = Utilities::buildBaseResponse(1074, "Hadees sub book not found.");
        } else {

            $obj = $hadeesBook->update($request->all());
            if ($obj) {
                $response = Utilities::buildBaseResponse(1073, "Hadees sub book successfully deleted.");
            }
        }

        return response()->json($response, $status);
    }

    /**
     * Get one Role.
     *
     * @param $id 'ID' of Role to return (required)
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getOneSetupHadeesSubBook($id, Request $request)
    {
        $mdlHadeesSubBook = new SetupHadeesSubBook();

        $request->request->add(['id' => $id]);

        $this->validate($request, $mdlHadeesSubBook->rules($request, Constant::RequestType['GET_ONE']), $mdlHadeesSubBook->messages($request, Constant::RequestType['GET_ONE']));

        $select = $this->select_columns;

        $mdlHadeesSubBook->filterColumns($request);

        if ($request->fields) {
            $select = $request->fields;
        }

        $hadeesBook = SetupHadeesSubBook::with('hadees_book')->where('id', $request->id)->first($select);

        $status = 200;
        $response = [];
        $dataResult = [];

        if (!$hadeesBook) {
            $status = 200;
            $response = Utilities::buildSuccessResponse(1074, "Hadees sub book not found.",$dataResult);
        } else {
            $status = 200;
            $dataResult = array("sub_hadees_book" => $hadeesBook->toArray());
            $response = Utilities::buildSuccessResponse(1075, "Hadees sub book data.", $dataResult);
        }

        return response()->json($response, $status);
    }

    /**
     * Fetch list of Role by searching with optional filters..
     *
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllSetupHadeesSubBook(Request $request)
    {
        $mdlHadeesSubBook = new SetupHadeesSubBook();

        $this->validate($request, $mdlHadeesSubBook->rules($request, Constant::RequestType['GET_ALL']), $mdlHadeesSubBook->messages($request, Constant::RequestType['GET_ALL']));

        $pageSize = $request->limit ?? ProjectConstant::PageSize;
        if ($pageSize > ProjectConstant::MaxPageSize) {
            $pageSize = ProjectConstant::PageSize;
        }
        $page = $request->page ?? ProjectConstant::Page;
        $skip = ($page - 1) * $pageSize;


        $select = $this->select_columns;

        $mdlHadeesSubBook->filterColumns($request);

        if ($request->fields) {
            $select = $request->fields;
        }

        $whereData = array();
        if ($request->book_name) {
            $whereData[] = ['book_name', 'LIKE', "%{$request->book_name}%"];
        }
        if ($request->hds_book_id) {
            $whereData[] = ['hds_book_id', '=', $request->hds_book_id];
        }
        if ($request->volume_id) {
            $whereData[] = ['volume_id', '=', $request->volume_id];
        }
        if ($request->activate != null) {
            $whereData[] = ['is_enable', $request->activate];
        }

        $hadeesBooksCount = SetupHadeesSubBook::with('hadees_book','volume')->where($whereData)->active()->get()->count();

        $orderBy =  $mdlHadeesSubBook->getOrderColumn($request->order_by);
        $orderType = $request->order_type ?? ProjectConstant::OrderType;

        $hadeesBooks = SetupHadeesSubBook::with('hadees_book','volume')->where($whereData)
        ->active()
        ->orderBy($orderBy, $orderType)
        ->offset($skip)
        ->limit($pageSize)
        ->get($select);

        $status = 200;
        $data_result = array("sub_hadees_book_total" => $hadeesBooksCount, "sub_hadees_book" => $hadeesBooks->toArray());
        $response = Utilities::buildSuccessResponse(1076, "Hadees sub book list.", $data_result);

        return response()->json($response, $status);
    }

}
