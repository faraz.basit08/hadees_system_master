<?php

namespace App\Http\Controllers;

use App\Config\ProjectConstant;
use App\Models\SetupMuhaddis;
use App\Models\UserRoles;
use App\Response\UserRolesResponse;
use DiUtil\Config\Constant;
use DiUtil\Utilities\Utilities;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller;

class SetupMuhaddisApi extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    private $select_columns = ['id', 'muhaddis_name', 'is_enable as activate'];

    /**
     * This fucntion is called after validation fails in function $this->validate.
     *
     * @param Request $request
     * @param array $errors
     * @return \Illuminate\Http\JsonResponse
     */

    protected function buildFailedValidationResponse(Request $request, array $errors){
        $response = Utilities::buildFailedValidationResponse(10000, "Unprocesssable Entity.", $errors);
        return response()->json($response, 400);
    }
    /**
     * Add Role.
     *
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function addSetupMuhaddis(Request $request)
    {
        $mdlMuhaddis = new SetupMuhaddis();

        $this->validate($request, $mdlMuhaddis->rules($request), $mdlMuhaddis->messages($request));

        $mdlMuhaddis->filterColumns($request);

        Utilities::defaultAddAttributes($request);

        $obj = SetupMuhaddis::create($request->all());

        $data = ['id' => $obj->id];

        $response = Utilities::buildSuccessResponse(1070, "SetupMuhaddis successfully created.", $data);

        return response()->json($response, 201);
    }

    /**
     * Update Role.
     *
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateSetupMuhaddis(Request $request)
    {
        $mdlMuhaddis = new SetupMuhaddis();

        $this->validate($request, $mdlMuhaddis->rules($request), $mdlMuhaddis->messages($request));

        $mdlMuhaddis->filterColumns($request);

        Utilities::defaultUpdateAttributes($request);

        $role = SetupMuhaddis::find($request->id);

        $status = 200;
        $response = [];

        if (!$role) {
            $status = 404;
            $response = Utilities::buildBaseResponse(1074, "SetupMuhaddis not found.");
        } else {

            $obj = $role->update($request->all());

            if ($obj) {
                $data = ['id' => $role->id];
                $response = Utilities::buildSuccessResponse(1071, "SetupMuhaddis successfully updated.", $data);
            }
        }

        return response()->json($response, $status);
    }

    /**
     * Activate/De-Activate Role.
     *
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function patchDisableEnableSetupMuhaddis(Request $request)
    {
        $mdlMuhaddis = new SetupMuhaddis();

        Utilities::removeAttributesExcept($request, ["id", "activate"]);

        $this->validate($request, $mdlMuhaddis->rules($request), $mdlMuhaddis->messages($request));

        Utilities::defaultUpdateAttributes($request);

        $activate = $request->activate == 1 ? Constant::RecordType['ENABLED'] : Constant::RecordType['DISABLED'];

        $request->request->add(['is_enable' => $activate]);

        $language = SetupMuhaddis::find($request->id);

        $status = 200;
        $response = [];

        if (!$language) {
            $status = 404;
            $response = Utilities::buildBaseResponse(1074, "SetupMuhaddis not found.");
        } else {

            $obj = $language->update($request->all());

            if ($obj) {
                $data = ['id' => $language->id];
                $actMsg = $request->activate == 1 ? "activated" : "de-activated";
                $response = Utilities::buildSuccessResponse(1072, "SetupMuhaddis successfully $actMsg.", $data);
            }
        }

        return response()->json($response, $status);
    }

    /**
     * Delete Role.
     *
     * @param $id 'ID' of Role to delete. (required)
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteSetupMuhaddis($id, Request $request)
    {
        $mdlMuhaddis = new SetupMuhaddis();

        $request->request->add(['id' => $id]);

        $this->validate($request, $mdlMuhaddis->rules($request), $mdlMuhaddis->messages($request));

        Utilities::defaultDeleteAttributes($request);

        $request->request->add(['is_enable' => Constant::RecordType['DELETED']]);

        $language = SetupMuhaddis::find($request->id);

        $status = 200;
        $response = [];
        if (!$language) {
            $status = 404;
            $response = Utilities::buildBaseResponse(1074, "SetupMuhaddis not found.");
        } else {

            $obj = $language->update($request->all());
            if ($obj) {
                $response = Utilities::buildBaseResponse(1073, "SetupMuhaddis successfully deleted.");
            }
        }

        return response()->json($response, $status);
    }

    /**
     * Get one Role.
     *
     * @param $id 'ID' of Role to return (required)
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getOneSetupMuhaddis($id, Request $request)
    {
        $mdlMuhaddis = new SetupMuhaddis();

        $request->request->add(['id' => $id]);

        $this->validate($request, $mdlMuhaddis->rules($request, Constant::RequestType['GET_ONE']), $mdlMuhaddis->messages($request, Constant::RequestType['GET_ONE']));

        $select = $this->select_columns;

        $mdlMuhaddis->filterColumns($request);

        if ($request->fields) {
            $select = $request->fields;
        }

        $muhaddis = SetupMuhaddis::where('id', $request->id)->first($select);

        $status = 200;
        $response = [];

        if (!$muhaddis) {
            $status = 404;
            $response = Utilities::buildBaseResponse(1074, "SetupMuhaddis not found.");
        } else {
            $status = 200;
            $dataResult = array("muhaddis" => $muhaddis->toArray());
            $response = Utilities::buildSuccessResponse(1075, "SetupMuhaddis data.", $dataResult);
        }

        return response()->json($response, $status);
    }

    /**
     * Fetch list of Role by searching with optional filters..
     *
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllSetupMuhaddis(Request $request)
    {
        $mdlMuhaddis = new SetupMuhaddis();

        $this->validate($request, $mdlMuhaddis->rules($request, Constant::RequestType['GET_ALL']), $mdlMuhaddis->messages($request, Constant::RequestType['GET_ALL']));

        $pageSize = $request->limit ?? ProjectConstant::PageSize;
        if ($pageSize > ProjectConstant::MaxPageSize) {
            $pageSize = ProjectConstant::PageSize;
        }
        $page = $request->page ?? ProjectConstant::Page;
        $skip = ($page - 1) * $pageSize;


        $select = $this->select_columns;

        $mdlMuhaddis->filterColumns($request);

        if ($request->fields) {
            $select = $request->fields;
        }

        $whereData = array();
        if ($request->muhaddis_name) {
            $whereData[] = ['muhaddis_name', 'LIKE', "%{$request->muhaddis_name}%"];
        }
        if ($request->activate != null) {
            $whereData[] = ['is_enable', $request->activate];
        }

        $muhaddisCount = SetupMuhaddis::where($whereData)->active()->get()->count();

        $orderBy =  $mdlMuhaddis->getOrderColumn($request->order_by);
        $orderType = $request->order_type ?? ProjectConstant::OrderType;

        $muhaddis = SetupMuhaddis::where($whereData)
        ->active()
        ->orderBy($orderBy, $orderType)
        ->offset($skip)
        ->limit($pageSize)
        ->get($select);

        $status = 200;
        $data_result = array("muhaddis_total" => $muhaddisCount, "muhaddis" => $muhaddis->toArray());
        $response = Utilities::buildSuccessResponse(1076, "SetupMuhaddis list.", $data_result);

        return response()->json($response, $status);
    }

}
