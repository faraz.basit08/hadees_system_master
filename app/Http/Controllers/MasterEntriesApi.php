<?php

namespace App\Http\Controllers;

use App\Config\ProjectConstant;
use App\Models\MasterEntries;
use App\Models\UserRoles;
use App\Response\UserRolesResponse;
use DiUtil\Config\Constant;
use DiUtil\Utilities\Utilities;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller;

class MasterEntriesApi extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    private $select_columns = ['id', 'cat_type','cat_val_1','cat_val_2','cat_val_3', 'is_enable as activate'];

    /**
     * This fucntion is called after validation fails in function $this->validate.
     *
     * @param Request $request
     * @param array $errors
     * @return \Illuminate\Http\JsonResponse
     */

    protected function buildFailedValidationResponse(Request $request, array $errors){
        $response = Utilities::buildFailedValidationResponse(10000, "Unprocesssable Entity.", $errors);
        return response()->json($response, 400);
    }
    /**
     * Add Role.
     *
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function addMasterEntries(Request $request)
    {
        $mdlMasterEntries = new MasterEntries();

        $this->validate($request, $mdlMasterEntries->rules($request), $mdlMasterEntries->messages($request));

        $mdlMasterEntries->filterColumns($request);

        Utilities::defaultAddAttributes($request);

        $obj = MasterEntries::create($request->all());

        $data = ['id' => $obj->id];

        $response = Utilities::buildSuccessResponse(1070, "Master entries successfully created.", $data);

        return response()->json($response, 201);
    }

    /**
     * Update Role.
     *
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateMasterEntries(Request $request)
    {
        $mdlMasterEntries = new MasterEntries();

        $this->validate($request, $mdlMasterEntries->rules($request), $mdlMasterEntries->messages($request));

        $mdlMasterEntries->filterColumns($request);

        Utilities::defaultUpdateAttributes($request);

        $role = MasterEntries::find($request->id);

        $status = 200;
        $response = [];

        if (!$role) {
            $status = 404;
            $response = Utilities::buildBaseResponse(1074, "Master entries not found.");
        } else {

            $obj = $role->update($request->all());

            if ($obj) {
                $data = ['id' => $role->id];
                $response = Utilities::buildSuccessResponse(1071, "Master entries successfully updated.", $data);
            }
        }

        return response()->json($response, $status);
    }

    /**
     * Activate/De-Activate Role.
     *
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function patchDisableEnableMasterEntries(Request $request)
    {
        $mdlMasterEntries = new MasterEntries();

        Utilities::removeAttributesExcept($request, ["id", "activate"]);

        $this->validate($request, $mdlMasterEntries->rules($request), $mdlMasterEntries->messages($request));

        Utilities::defaultUpdateAttributes($request);

        $activate = $request->activate == 1 ? Constant::RecordType['ENABLED'] : Constant::RecordType['DISABLED'];

        $request->request->add(['is_enable' => $activate]);

        $hadeesBook = MasterEntries::find($request->id);

        $status = 200;
        $response = [];

        if (!$hadeesBook) {
            $status = 404;
            $response = Utilities::buildBaseResponse(1074, "Master entries not found.");
        } else {

            $obj = $hadeesBook->update($request->all());

            if ($obj) {
                $data = ['id' => $hadeesBook->id];
                $actMsg = $request->activate == 1 ? "activated" : "de-activated";
                $response = Utilities::buildSuccessResponse(1072, "Master entries successfully $actMsg.", $data);
            }
        }

        return response()->json($response, $status);
    }

    /**
     * Delete Role.
     *
     * @param $id 'ID' of Role to delete. (required)
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteMasterEntries($id, Request $request)
    {
        $mdlMasterEntries = new MasterEntries();

        $request->request->add(['id' => $id]);

        $this->validate($request, $mdlMasterEntries->rules($request), $mdlMasterEntries->messages($request));

        Utilities::defaultDeleteAttributes($request);

        $request->request->add(['is_enable' => Constant::RecordType['DELETED']]);

        $hadeesBook = MasterEntries::find($request->id);

        $status = 200;
        $response = [];
        if (!$hadeesBook) {
            $status = 404;
            $response = Utilities::buildBaseResponse(1074, "Master entries not found.");
        } else {

            $obj = $hadeesBook->update($request->all());
            if ($obj) {
                $response = Utilities::buildBaseResponse(1073, "Master entries successfully deleted.");
            }
        }

        return response()->json($response, $status);
    }

    /**
     * Get one Role.
     *
     * @param $id 'ID' of Role to return (required)
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getOneMasterEntries($id, Request $request)
    {
        $mdlMasterEntries = new MasterEntries();

        $request->request->add(['id' => $id]);

        $this->validate($request, $mdlMasterEntries->rules($request, Constant::RequestType['GET_ONE']), $mdlMasterEntries->messages($request, Constant::RequestType['GET_ONE']));

        $select = $this->select_columns;

        $mdlMasterEntries->filterColumns($request);

        if ($request->fields) {
            $select = $request->fields;
        }

        $hadeesBook = MasterEntries::where('id', $request->id)->first($select);

        $status = 200;
        $response = [];

        if (!$hadeesBook) {
            $status = 404;
            $response = Utilities::buildBaseResponse(1074, "Master entries not found.");
        } else {
            $status = 200;
            $dataResult = array("master_entries" => $hadeesBook->toArray());
            $response = Utilities::buildSuccessResponse(1075, "Master entries data.", $dataResult);
        }

        return response()->json($response, $status);
    }

    /**
     * Fetch list of Role by searching with optional filters..
     *
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllMasterEntries(Request $request)
    {
        $mdlMasterEntries = new MasterEntries();

        $this->validate($request, $mdlMasterEntries->rules($request, Constant::RequestType['GET_ALL']), $mdlMasterEntries->messages($request, Constant::RequestType['GET_ALL']));

        $pageSize = $request->limit ?? ProjectConstant::PageSize;
        if ($pageSize > ProjectConstant::MaxPageSize) {
            $pageSize = ProjectConstant::PageSize;
        }
        $page = $request->page ?? ProjectConstant::Page;
        $skip = ($page - 1) * $pageSize;


        $select = $this->select_columns;

        $mdlMasterEntries->filterColumns($request);

        if ($request->fields) {
            $select = $request->fields;
        }

        $whereData = array();
        if ($request->cat_type) {
            $whereData[] = ['cat_type', 'LIKE', "%{$request->cat_type}%"];
        }
        if ($request->cat_val_1) {
            $whereData[] = ['cat_val_1', 'LIKE', "%{$request->cat_val_1}%"];
        }
        if ($request->cat_val_2) {
            $whereData[] = ['cat_val_2', 'LIKE', "%{$request->cat_val_2}%"];
        }
        if ($request->cat_val_3) {
            $whereData[] = ['cat_val_3', 'LIKE', "%{$request->cat_val_3}%"];
        }
        if ($request->activate != null) {
            $whereData[] = ['is_enable', $request->activate];
        }

        $masterEntriesCount = MasterEntries::where($whereData)->active()->get()->count();

        $orderBy =  $mdlMasterEntries->getOrderColumn($request->order_by);
        $orderType = $request->order_type ?? ProjectConstant::OrderType;

        $masterEntries = MasterEntries::where($whereData)
        ->active()
        ->orderBy($orderBy, $orderType)
        ->offset($skip)
        ->limit($pageSize)
        ->get($select);

        $status = 200;
        $data_result = array("master_entries_total" => $masterEntriesCount, "master_entries" => $masterEntries->toArray());
        $response = Utilities::buildSuccessResponse(1076, "Master entries list.", $data_result);

        return response()->json($response, $status);
    }

}
