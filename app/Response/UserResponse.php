<?php
namespace App\Response;

class UserResponse
{

    private $users;
    private $totalUsers;

    /**
     *
     * @return array
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     *
     * @param array $users
     */
    public function setUsers($users)
    {
        $this->users = $users;
    }

    /**
     *
     * @return array
     */
    public function getTotalUsers()
    {
        return $this->totalUsers;
    }

    /**
     *
     * @param array $totalUsers
     */
    public function setTotalUsers($totalUsers)
    {
        $this->totalUsers = $totalUsers;
    }
}
