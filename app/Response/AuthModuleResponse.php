<?php
namespace App\Response;

class AuthModuleResponse
{

    private $modules;
    private $totalModules;

    /**
     *
     * @return array
     */
    public function getModules()
    {
        return $this->modules;
    }

    /**
     *
     * @param array $modules
     */
    public function setModules($modules)
    {
        $this->modules = $modules;
    }

    /**
     *
     * @return array $modulesCount
     */
    public function getTotalModules()
    {
        return $this->totalModules;
    }

    /**
     *
     * @param array $totalModules
     */
    public function setTotalModules($totalModules)
    {
        $this->totalModules = $totalModules;
    }
}
