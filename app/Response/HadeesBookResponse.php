<?php
namespace App\Response;

class HadeesBookResponse
{

    private $hadees;
    private $totalHadeesBooks;

    /**
     *
     * @return array
     */
    public function getHadeesBook()
    {
        return $this->hadees;
    }

    /**
     *
     * @param array $roles
     */
    public function setHadeesBook($roles)
    {
        $this->hadees = $roles;
    }

    /**
     *
     * @return array
     */
    public function getTotalHadeesBook()
    {
        return $this->totalHadeesBooks;
    }

    /**
     *
     * @param array $totalHadeesBooks
     */
    public function setTotalHadeesBook($totalHadeesBooks)
    {
        $this->totalHadeesBooks = $totalHadeesBooks;
    }
}
