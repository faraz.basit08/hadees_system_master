<?php

namespace App\Models;

use DiUtil\Config\Constant;
use DiUtil\Utilities\Utilities;
use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Laravel\Lumen\Auth\Authorizable;

class HadeesMuhaddisMapping extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable, HasFactory;

    protected $table = 'hadees_muhaddis_mapping';

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'id',
        'muhaddis_id',
        'hadees_book_id',
        'muhaddis_intro',
        'dob',
        'dou',
        'is_enable',
        'created_by',
        'created_at',
        'updated_by',
        'updated_at',

    ];
    protected $tableColumnList = [
        'id' => 'id',
        'muhaddis_id',
        'hadees_book_id',
        'muhaddis_intro',
        'is_enable',
        'dob',
        'dou',
        'activate' => 'is_enable',
        'created_by' => 'created_by',
        'created_at' => 'created_at',
        'updated_by' => 'updated_by',
        'updated_at' => 'updated_at',
    ];


    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var string[]
     */
    protected $hidden = [

    ];
    protected $otherColumnList = [];
    protected $columnList = [];

    /**
     * Scope a query to only include active records.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query

     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('is_enable', '!=', Constant::RecordType['DELETED']);
    }

    public function filterColumns(Request $request, $method = null)
    {
        if ($method == null) {
            $method = $request->method();
        }

        $columnList = $this->tableColumnList;

        Utilities::filterColumnsModel($request, $columnList, $method);
    }
    /**
     * Get column for ordering after varification.
     *
     * @param string $field
     * @return string[]|array|string
     */
    public function getOrderColumn($field)
    {
        $columnList = $this->tableColumnList;

        foreach ($columnList as $key => $value) {
            if ($key === $field)
                return $value;
        }

        return "id";
    }
    public function rules($request, $method = null)
    {
        if ($method == null) {
            $method = $request->method();
        }

        $rules = [];

        $rules = match ($method) {
            'POST' => [
                'muhaddis_id' => 'required|integer',
                'hadees_book_id' => 'required|integer',
            ],
            'PUT' => [
                'id' => 'required|integer',
                'muhaddis_id' => 'required|integer',
                'hadees_book_id' => 'required|integer',
            ],
            'PATCH' => [
                'id' => 'required|integer',
                'activate' => 'required|numeric|between:0,1'
            ],
            'DELETE' => [
                'id' => 'required|integer',
            ],
            'GET_ONE' => [
                'id' => 'required|integer'
                // 'fields' => ''
            ],
            'GET_ALL' => [
                // 'fields' => ''
            ]
        };

        return $rules;
    }

    /**
     * Get the validation custom messages.
     *
     * @return array
     */
    public function messages($request, $method = null)
    {
        if ($method == null) {
            $method = $request->method();
        }

        $messages = [];

        $commonMessages = [
            'muhaddis_id.required' => [
                "code" => 10418,
                "message" => "Please provide SetupMuhaddis id."
            ],

            'muhaddis_id.integer' => [
                "code" => 10418,
                "message" => "SetupMuhaddis id must be an integer."
            ],
            'hadees_book_id.required' => [
                "code" => 10418,
                "message" => "Please provide hadees book id id."
            ],
            'hadees_book_id.integer' => [
                "code" => 10418,
                "message" => "Hadees book id must be an integer."
            ],
        ];

        $idMessages = [
            'id.required' => [
                "code" => 10433,
                "message" => "Please provide mapping id."
            ],
            'id.integer' => [
                "code" => 10434,
                "message" => "Id must be an integer."
            ]
        ];

        $statusMessage = [
            'activate.required' => [
                "code" => 10435,
                "message" => "Please provide activate flag."
            ],
            'activate.numeric' => [
                "code" => 10436,
                "message" => "Activate flag must be an integer."
            ],
            'activate.between' => [
                'numeric' => [
                    "code" => 10437,
                    "message" => "The activate flag must be between :min and :max."
                ]
            ]
        ];

        $messages = match ($method) {
            'POST' => $commonMessages,
            'PUT' => $commonMessages + $idMessages,
            'PATCH' => $idMessages + $statusMessage,
            'DELETE' => $idMessages,
            'GET_ONE' => $idMessages,
            'GET_ALL' => $messages = []
        };

        return $messages;
    }

}
