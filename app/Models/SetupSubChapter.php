<?php

namespace App\Models;

use DiUtil\Config\Constant;
use DiUtil\Utilities\Utilities;
use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Validation\Rule;

class SetupSubChapter extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable, HasFactory;

    protected $table = 'stp_sub_chapter';

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'id',
        'hds_book_id',
        'volume_id',
        'hds_sub_book_id',
        'chap_id',
        'sub_chapter',
        'is_enable',
        'created_by',
        'created_at',
        'updated_by',
        'updated_at',

    ];
    protected $tableColumnList = [
        'id' => 'id',
        'hds_book_id' => 'hds_book_id',
        'volume_id' => 'volume_id',
        'hds_sub_book_id' => 'hds_sub_book_id',
        'chap_id' => 'chap_id',
        'sub_chapter' => 'sub_chapter',
        'activate' => 'is_enable',
        'created_by' => 'created_by',
        'created_at' => 'created_at',
        'updated_by' => 'updated_by',
        'updated_at' => 'updated_at',
    ];


    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var string[]
     */
    protected $hidden = [

    ];
    protected $otherColumnList = [];
    protected $columnList = [];

    public function hadees_book()
    {
        return $this->belongsTo(SetupHadeesBook::class, 'hds_book_id')->where('is_enable',1)->select('id', 'book_name');
    }
    public function hadees_sub_book()
    {
        return $this->belongsTo(SetupHadeesSubBook::class, 'hds_sub_book_id')->where('is_enable',1)->select('id', 'sub_book_name');
    }
    public function hadees_volume()
    {
        return $this->belongsTo(HadeesVolume::class, 'volume_id')->where('is_enable',1)->select('id', 'vol_number');
    }
    public function setup_chapter()
    {
        return $this->belongsTo(SetupChapter::class, 'chap_id')->where('is_enable',1)->select('id', 'chap_name');
    }


    /**
     * Scope a query to only include active records.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('is_enable', '!=', Constant::RecordType['DELETED']);
    }

    public function filterColumns(Request $request, $method = null)
    {
        if ($method == null) {
            $method = $request->method();
        }

        $columnList = $this->tableColumnList;

        Utilities::filterColumnsModel($request, $columnList, $method);
    }

    /**
     * Get column for ordering after varification.
     *
     * @param string $field
     * @return string[]|array|string
     */
    public function getOrderColumn($field)
    {
        $columnList = $this->tableColumnList;

        foreach ($columnList as $key => $value) {
            if ($key === $field)
                return $value;
        }

        return "id";
    }

    public function rules($request, $method = null)
    {
        if ($method == null) {
            $method = $request->method();
        }

        $rules = [];

        $rules = match ($method) {
            'POST' => [
                // 'sub_chapter' => 'required',
                'sub_chapter' => [
                    'required',
                    //  Rule::unique($this->table, 'sub_chapter')->where(function ($query) use ($request) {
                    //     $query->where('is_enable', '<>', '2');
                    // })
                ],
            ],
            'PUT' => [
                'id' => 'required|integer',
                'sub_chapter' => [
                    'required',
                    //  Rule::unique($this->table, 'sub_chapter')->where(function ($query) use ($request) {
                    //     $query->where('is_enable', '<>', '2');
                    // })
                ],
            ],
            'PATCH' => [
                'id' => 'required|integer',
                'activate' => 'required|numeric|between:0,1'
            ],
            'DELETE' => [
                'id' => 'required|integer',
            ],
            'GET_ONE' => [
                'id' => 'required|integer'
                // 'fields' => ''
            ],
            'GET_ALL' => [
                // 'fields' => ''
            ]
        };

        return $rules;
    }

    /**
     * Get the validation custom messages.
     *
     * @return array
     */
    public function messages($request, $method = null)
    {
        if ($method == null) {
            $method = $request->method();
        }

        $messages = [];

        $commonMessages = [
            'nratr_name.required' => [
                "code" => 10418,
                "message" => "Please provide sub chapter name."
            ],
            'tabqa_id.integer' => [
                "code" => 10418,
                "message" => "Tabqa id must be integer."
            ],
        ];

        $idMessages = [
            'id.required' => [
                "code" => 10433,
                "message" => "Please provide sub chapter id."
            ],
            'id.integer' => [
                "code" => 10434,
                "message" => "Id must be an integer."
            ]
        ];

        $statusMessage = [
            'activate.required' => [
                "code" => 10435,
                "message" => "Please provide activate flag."
            ],
            'activate.numeric' => [
                "code" => 10436,
                "message" => "Activate flag must be an integer."
            ],
            'activate.between' => [
                'numeric' => [
                    "code" => 10437,
                    "message" => "The activate flag must be between :min and :max."
                ]
            ]
        ];

        $messages = match ($method) {
            'POST' => $commonMessages,
            'PUT' => $commonMessages + $idMessages,
            'PATCH' => $idMessages + $statusMessage,
            'DELETE' => $idMessages,
            'GET_ONE' => $idMessages,
            'GET_ALL' => $messages = []
        };

        return $messages;
    }

}
