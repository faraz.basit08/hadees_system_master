<?php

namespace App\Models;

use DiUtil\Config\Constant;
use DiUtil\Utilities\Utilities;
use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Laravel\Lumen\Auth\Authorizable;

class SetupKeyword extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable, HasFactory;

    protected $table = 'stp_keyword';

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'id',
        'keyword_name',
        'language_id',
        'is_enable',
        'created_by',
        'created_at',
        'updated_by',
        'updated_at',

    ];
    protected $tableColumnList = [
        'id' => 'id',
        'keyword_name' => 'keyword_name',
        'language_id' => 'language_id',
        'activate' => 'is_enable',
        'created_by' => 'created_by',
        'created_at' => 'created_at',
        'updated_by' => 'updated_by',
        'updated_at' => 'updated_at',
    ];

    public function language()
    {
        return $this->belongsTo(SetupLanguage::class, 'language_id')->where('is_enable',1)->select('id', 'language_name');
    }
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var string[]
     */
    protected $hidden = [

    ];
    protected $otherColumnList = [];
    protected $columnList = [];

    /**
     * Scope a query to only include active records.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query

     * @return \Illuminate\Database\Eloquent\Builder
     */

    public function scopeActive($query)
    {
        return $query->where('is_enable', '!=', Constant::RecordType['DELETED']);
    }

    public function filterColumns(Request $request, $method = null)
    {
        if ($method == null) {
            $method = $request->method();
        }

        $columnList = $this->tableColumnList;

        Utilities::filterColumnsModel($request, $columnList, $method);
    }
    /**
     * Get column for ordering after varification.
     *
     * @param string $field
     * @return string[]|array|string
     */
    public function getOrderColumn($field)
    {
        $columnList = $this->tableColumnList;

        foreach ($columnList as $key => $value) {
            if ($key === $field)
                return $value;
        }

        return "id";
    }
    public function rules($request, $method = null)
    {
        if ($method == null) {
            $method = $request->method();
        }

        $rules = [];

        $rules = match ($method) {
            'POST' => [
                // 'keyword_name' => 'required',
                'keyword_name' => [
                    'required', Rule::unique($this->table, 'keyword_name')->where(function ($query) use ($request) {
                        $query->where('is_enable', '<>', '2');
                    })
                ],
                'language_id' => 'required|integer',
            ],
            'PUT' => [
                'id' => 'required|integer',
                'keyword_name' => [
                    'required', Rule::unique($this->table, 'keyword_name')->where(function ($query) use ($request) {
                        $query->where('is_enable', '<>', '2');
                    })
                ],
                'language_id' => 'required|integer',
            ],
            'PATCH' => [
                'id' => 'required|integer',
                'activate' => 'required|numeric|between:0,1'
            ],
            'DELETE' => [
                'id' => 'required|integer',
            ],
            'GET_ONE' => [
                'id' => 'required|integer'
                // 'fields' => ''
            ],
            'GET_ALL' => [
                // 'fields' => ''
            ]
        };

        return $rules;
    }

    /**
     * Get the validation custom messages.
     *
     * @return array
     */
    public function messages($request, $method = null)
    {
        if ($method == null) {
            $method = $request->method();
        }

        $messages = [];

        $commonMessages = [
            'keyword_name.required' => [
                "code" => 10418,
                "message" => "Please provide language name."
            ],
            'language_id.required' => [
                "code" => 10418,
                "message" => "Please provide keyword name."
            ],
            'language_id.integer' => [
                "code" => 10418,
                "message" => "Language id must be an integer."
            ],
        ];

        $idMessages = [
            'id.required' => [
                "code" => 10433,
                "message" => "Please provide language id."
            ],
            'id.integer' => [
                "code" => 10434,
                "message" => "Id must be an integer."
            ]
        ];

        $statusMessage = [
            'activate.required' => [
                "code" => 10435,
                "message" => "Please provide activate flag."
            ],
            'activate.numeric' => [
                "code" => 10436,
                "message" => "Activate flag must be an integer."
            ],
            'activate.between' => [
                'numeric' => [
                    "code" => 10437,
                    "message" => "The activate flag must be between :min and :max."
                ]
            ]
        ];

        $messages = match ($method) {
            'POST' => $commonMessages,
            'PUT' => $commonMessages + $idMessages,
            'PATCH' => $idMessages + $statusMessage,
            'DELETE' => $idMessages,
            'GET_ONE' => $idMessages,
            'GET_ALL' => $messages = []
        };

        return $messages;
    }

}
