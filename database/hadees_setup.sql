/*
SQLyog Ultimate v13.1.1 (32 bit)
MySQL - 10.4.18-MariaDB : Database - hadees_setups
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`hadees_setups` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `hadees_setups`;

/*Table structure for table `book_volume_mapping` */

DROP TABLE IF EXISTS `book_volume_mapping`;

CREATE TABLE `book_volume_mapping` (
  `hds_book_id` int(11) DEFAULT NULL,
  `volume_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `book_volume_mapping` */

/*Table structure for table `master_entries` */

DROP TABLE IF EXISTS `master_entries`;

CREATE TABLE `master_entries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_type` varchar(255) DEFAULT NULL,
  `cat_val_1` varchar(255) DEFAULT NULL,
  `cat_val_2` varchar(255) DEFAULT NULL,
  `cat_val_3` varchar(255) DEFAULT NULL,
  `is_enable` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

/*Data for the table `master_entries` */

insert  into `master_entries`(`id`,`cat_type`,`cat_val_1`,`cat_val_2`,`cat_val_3`,`is_enable`,`created_at`,`updated_at`,`created_by`,`updated_by`,`deleted_at`) values 
(1,'ماسٹر اندراجات کا نام ','ماسٹر اندراجات کیٹ ویل ون','ماسٹر اندراجات کیٹ ویل ٹو','ماسٹر اندراجات کیٹ ویل تھری ',1,'2022-04-13 12:27:35','2022-04-13 07:27:35',NULL,NULL,NULL);

/*Table structure for table `stp_chapter` */

DROP TABLE IF EXISTS `stp_chapter`;

CREATE TABLE `stp_chapter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hds_book_id` int(11) DEFAULT NULL,
  `hds_sub_book_id` int(11) DEFAULT NULL,
  `volume_id` int(11) DEFAULT NULL,
  `chap_name` varchar(300) DEFAULT NULL,
  `is_enable` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `hadees_book_chfk_1` (`hds_book_id`),
  KEY `hadees_sub_book_chfk_2` (`hds_sub_book_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

/*Data for the table `stp_chapter` */

insert  into `stp_chapter`(`id`,`hds_book_id`,`hds_sub_book_id`,`volume_id`,`chap_name`,`is_enable`,`created_at`,`created_by`,`updated_at`,`updated_by`) values 
(1,1,0,8,'باب کا نام',1,'2022-04-11 10:44:30',NULL,'2022-04-11 10:44:30',NULL),
(2,1,1,7,'باب کا نام ',1,'2022-04-11 10:50:00',NULL,'2022-04-11 10:50:00',NULL),
(3,3,2,9,'حرفِ اوّل',1,'2022-04-12 08:21:23',NULL,'2022-04-12 08:21:23',NULL),
(4,8,4,12,'	كتاب الايمان',1,'2022-04-18 06:58:10',NULL,'2022-04-18 06:58:10',NULL);

/*Table structure for table `stp_hadees` */

DROP TABLE IF EXISTS `stp_hadees`;

CREATE TABLE `stp_hadees` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hds_book_id` int(11) DEFAULT NULL,
  `hds_sub_book_id` int(11) DEFAULT NULL,
  `volume_id` int(11) DEFAULT NULL,
  `chap_id` int(11) DEFAULT NULL,
  `narrator_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `hadees_no` varchar(50) DEFAULT NULL,
  `with_araab` text DEFAULT NULL,
  `without_araab` text DEFAULT NULL,
  `reference` varchar(255) DEFAULT NULL,
  `is_enable` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT current_timestamp(),
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `stp_hadees` */

/*Table structure for table `stp_hadees_book` */

DROP TABLE IF EXISTS `stp_hadees_book`;

CREATE TABLE `stp_hadees_book` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `book_name` varchar(2055) DEFAULT NULL,
  `is_enable` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4;

/*Data for the table `stp_hadees_book` */

insert  into `stp_hadees_book`(`id`,`book_name`,`is_enable`,`created_at`,`created_by`,`updated_at`,`updated_by`) values 
(1,'Sahih al-Bukhari',1,'2022-03-24 10:09:35',NULL,'2022-03-24 15:10:37',NULL),
(2,'صحیح البخاری	',1,'2022-04-12 08:16:48',NULL,'2022-04-12 08:16:48',NULL),
(3,'مشکوٰۃ المصابیح	',1,'2022-04-12 08:17:13',NULL,'2022-04-12 08:17:13',NULL),
(4,'مشکوٰۃ المصابیح',1,'2022-04-15 08:02:59',NULL,'2022-04-15 08:03:00',NULL),
(5,'	مشکوٰۃ المصابیح',1,'2022-04-15 08:05:19',NULL,'2022-04-15 08:05:19',NULL),
(6,'مشکوٰۃ المصابیح',1,'2022-04-15 08:11:01',NULL,'2022-04-15 08:11:01',NULL),
(7,'مشکوٰۃ المصابیح',1,'2022-04-15 08:12:25',NULL,'2022-04-15 08:12:25',NULL),
(8,'مشکوٰۃ المصابیح	',1,'2022-04-18 06:03:29',NULL,'2022-04-18 06:03:29',NULL),
(9,'Sahih al-Bukhari	',1,'2022-04-18 06:03:48',NULL,'2022-04-18 06:03:49',NULL),
(10,'حدیث کی کتاب',1,'2022-04-18 06:43:04',NULL,'2022-04-18 06:43:04',NULL),
(11,'حدیث کی کتاب کا نام ',1,'2022-04-18 06:50:32',NULL,'2022-04-18 06:50:32',NULL),
(12,'حدیث کی کتاب کا نام	',1,'2022-04-18 07:45:38',NULL,'2022-04-18 07:45:38',NULL),
(13,'حدیث کی کتاب کا نام1',1,'2022-04-19 06:03:11',NULL,'2022-04-19 06:03:11',NULL),
(14,'حدیث کی کتاب کا نام1	',1,'2022-04-19 06:03:22',NULL,'2022-04-19 06:03:22',NULL),
(15,'صحیح البخاری',1,'2022-04-19 06:04:51',NULL,'2022-04-19 06:04:51',NULL),
(16,'sed',1,'2022-04-19 06:05:24',NULL,'2022-04-19 06:05:24',NULL),
(17,'	حدیث کی  sdaf',1,'2022-04-19 07:37:06',NULL,'2022-04-19 07:39:07',NULL);

/*Table structure for table `stp_hadees_sub_book` */

DROP TABLE IF EXISTS `stp_hadees_sub_book`;

CREATE TABLE `stp_hadees_sub_book` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hds_book_id` int(11) DEFAULT NULL,
  `volume_id` int(11) DEFAULT NULL,
  `sub_book_name` varchar(2025) DEFAULT NULL,
  `is_enable` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `hadees_book_fk_1` (`hds_book_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

/*Data for the table `stp_hadees_sub_book` */

insert  into `stp_hadees_sub_book`(`id`,`hds_book_id`,`volume_id`,`sub_book_name`,`is_enable`,`created_at`,`created_by`,`updated_at`,`updated_by`) values 
(1,1,7,'Sub Book (Sahih al-Bukhari)',1,'2022-03-24 10:11:14',NULL,'2022-04-11 10:49:40',NULL),
(2,3,9,'كتاب الايمان	',1,'2022-04-12 08:18:03',NULL,'2022-04-12 08:18:03',NULL),
(3,8,12,'	كتاب الايمان',1,'2022-04-18 06:56:42',NULL,'2022-04-18 06:56:42',NULL),
(4,8,12,'	كتاب الايمان',1,'2022-04-18 06:56:51',NULL,'2022-04-18 06:56:51',NULL);

/*Table structure for table `stp_keyword` */

DROP TABLE IF EXISTS `stp_keyword`;

CREATE TABLE `stp_keyword` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `keyword_name` varchar(300) DEFAULT NULL,
  `language_id` int(11) DEFAULT NULL,
  `is_enable` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

/*Data for the table `stp_keyword` */

insert  into `stp_keyword`(`id`,`keyword_name`,`language_id`,`is_enable`,`created_at`,`created_by`,`updated_at`,`updated_by`) values 
(1,'کی ورڈ کا نام ',3,1,'2022-04-13 11:23:54',NULL,'2022-04-13 11:23:54',NULL);

/*Table structure for table `stp_keyword_details` */

DROP TABLE IF EXISTS `stp_keyword_details`;

CREATE TABLE `stp_keyword_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `keyword_id` int(11) DEFAULT NULL,
  `keyword_detail` varchar(100) DEFAULT NULL,
  `language_id` int(11) DEFAULT NULL,
  `is_enable` tinyint(4) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;

/*Data for the table `stp_keyword_details` */

insert  into `stp_keyword_details`(`id`,`keyword_id`,`keyword_detail`,`language_id`,`is_enable`,`created_at`,`created_by`,`updated_at`,`updated_by`) values 
(2,2,'usama entry ',2,2,'2022-04-07 08:27:28',NULL,'2022-04-13 11:24:34',NULL),
(3,2,'keyword detail saefs',1,2,'2022-04-12 10:49:16',NULL,'2022-04-13 11:24:31',NULL),
(4,3,'keyword detail',1,2,'2022-04-12 11:16:12',NULL,'2022-04-13 11:24:28',NULL),
(5,3,'keyword detail',1,2,'2022-04-12 11:17:33',NULL,'2022-04-13 11:24:25',NULL),
(6,1,'keyword detail',3,1,'2022-04-12 11:22:13',NULL,'2022-04-13 11:24:07',NULL);

/*Table structure for table `stp_language` */

DROP TABLE IF EXISTS `stp_language`;

CREATE TABLE `stp_language` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `language_name` varchar(200) DEFAULT NULL,
  `is_enable` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

/*Data for the table `stp_language` */

insert  into `stp_language`(`id`,`language_name`,`is_enable`,`created_at`,`created_by`,`updated_at`,`updated_by`) values 
(1,'arabic',1,'2022-04-12 09:11:05',NULL,'2022-04-12 09:17:49',NULL),
(2,'urdu',1,'2022-04-12 09:11:49',NULL,'2022-04-12 09:11:49',NULL),
(3,'farsi',1,'2022-04-12 12:07:06',NULL,'2022-04-12 12:07:06',NULL);

/*Table structure for table `stp_muhaddiseen` */

DROP TABLE IF EXISTS `stp_muhaddiseen`;

CREATE TABLE `stp_muhaddiseen` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `muhaddis_name` varchar(900) DEFAULT NULL,
  `is_enable` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

/*Data for the table `stp_muhaddiseen` */

insert  into `stp_muhaddiseen`(`id`,`muhaddis_name`,`is_enable`,`created_at`,`created_by`,`updated_at`,`updated_by`) values 
(1,'محدثین کا نام dsf',1,'2022-04-12 16:48:36',NULL,'2022-04-12 11:48:36',NULL);

/*Table structure for table `stp_narrator` */

DROP TABLE IF EXISTS `stp_narrator`;

CREATE TABLE `stp_narrator` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tabqa_id` int(11) DEFAULT NULL,
  `nratr_name` varchar(300) DEFAULT NULL,
  `father_name` varchar(300) DEFAULT NULL,
  `kuniyat` varchar(300) DEFAULT NULL,
  `nasab` varchar(300) DEFAULT NULL,
  `laqab` varchar(300) DEFAULT NULL,
  `nratr_dob` date DEFAULT NULL,
  `nratr_pob` varchar(300) DEFAULT NULL,
  `nratr_dod` date DEFAULT NULL,
  `nratr_pod` varchar(300) DEFAULT NULL,
  `nratr_teachers` varchar(765) DEFAULT NULL,
  `nratr_student` varchar(765) DEFAULT NULL,
  `is_enable` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

/*Data for the table `stp_narrator` */

insert  into `stp_narrator`(`id`,`tabqa_id`,`nratr_name`,`father_name`,`kuniyat`,`nasab`,`laqab`,`nratr_dob`,`nratr_pob`,`nratr_dod`,`nratr_pod`,`nratr_teachers`,`nratr_student`,`is_enable`,`created_at`,`created_by`,`updated_at`,`updated_by`) values 
(1,NULL,'Umar bin Al-Khattab','ABC Father',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,2,'2022-04-13 15:29:24',NULL,'2022-04-13 10:29:24',NULL),
(2,1,'راوی کا نام ','راوی کے والد کا نام ','کنیت','نسب ','لقب','1992-04-01','پیدائش کی جگہ ','2022-04-13','وفات کی جگہ ','اساتذہ','تالامذہ',1,'2022-04-14 11:21:06',NULL,'2022-04-14 06:21:06',NULL),
(3,33,'سید ممتاز شاہ','سید ممتاز شاہ','کنیت','نسب ','لقب','2022-04-14','پیدائش کی جگہ ','2022-04-15','وفات کی جگہ ','اساتذہ','اساتذہ ',1,'2022-04-14 11:25:29',NULL,'2022-04-14 06:25:29',NULL);

/*Table structure for table `stp_sharah` */

DROP TABLE IF EXISTS `stp_sharah`;

CREATE TABLE `stp_sharah` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sharah_name` varchar(300) DEFAULT NULL,
  `is_enable` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

/*Data for the table `stp_sharah` */

insert  into `stp_sharah`(`id`,`sharah_name`,`is_enable`,`created_at`,`created_by`,`updated_at`,`updated_by`) values 
(1,'Hadees Sharah',1,'2022-03-24 15:13:11',NULL,'2022-03-24 10:13:02',NULL);

/*Table structure for table `stp_shareh` */

DROP TABLE IF EXISTS `stp_shareh`;

CREATE TABLE `stp_shareh` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shreh_name` varchar(300) DEFAULT NULL,
  `is_enable` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

/*Data for the table `stp_shareh` */

insert  into `stp_shareh`(`id`,`shreh_name`,`is_enable`,`created_at`,`created_by`,`updated_at`,`updated_by`) values 
(1,'Hadees Shareh asdfs',1,'2022-03-24 15:17:26',NULL,'2022-03-24 10:17:05',NULL);

/*Table structure for table `stp_sub_chapter` */

DROP TABLE IF EXISTS `stp_sub_chapter`;

CREATE TABLE `stp_sub_chapter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hds_book_id` int(11) DEFAULT NULL,
  `hds_sub_book_id` int(11) DEFAULT NULL,
  `volume_id` int(11) DEFAULT NULL,
  `chap_id` int(11) DEFAULT NULL,
  `sub_chapter` varchar(300) DEFAULT NULL,
  `is_enable` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

/*Data for the table `stp_sub_chapter` */

insert  into `stp_sub_chapter`(`id`,`hds_book_id`,`hds_sub_book_id`,`volume_id`,`chap_id`,`sub_chapter`,`is_enable`,`created_at`,`created_by`,`updated_at`,`updated_by`) values 
(1,3,2,9,3,'ضمنی باب کا نام ',1,'2022-04-13 09:59:53',NULL,'2022-04-13 09:59:53',NULL),
(2,3,2,9,3,'ضمنی باب کا نام 2',1,'2022-04-13 15:15:06',NULL,'2022-04-13 10:15:06',NULL),
(3,3,2,9,3,'ضمنی باب کا نام	',1,'2022-04-18 08:07:30',NULL,'2022-04-18 08:07:30',NULL);

/*Table structure for table `stp_sub_topic` */

DROP TABLE IF EXISTS `stp_sub_topic`;

CREATE TABLE `stp_sub_topic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `topic_id` int(11) DEFAULT NULL,
  `hds_book_id` int(11) DEFAULT NULL,
  `hds_sub_book_id` int(11) DEFAULT NULL,
  `volume_id` int(11) DEFAULT NULL,
  `chap_id` int(11) DEFAULT NULL,
  `sub_topic_name` varchar(300) DEFAULT NULL,
  `is_enable` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

/*Data for the table `stp_sub_topic` */

insert  into `stp_sub_topic`(`id`,`topic_id`,`hds_book_id`,`hds_sub_book_id`,`volume_id`,`chap_id`,`sub_topic_name`,`is_enable`,`created_at`,`created_by`,`updated_at`,`updated_by`) values 
(1,NULL,NULL,NULL,NULL,NULL,NULL,2,'2022-04-12 16:50:14',NULL,'2022-04-12 11:50:14',NULL),
(2,2,NULL,NULL,NULL,NULL,'Sub topic of topic 2',1,'2022-04-12 11:33:44',NULL,'2022-04-12 11:33:44',NULL),
(3,2,NULL,NULL,NULL,NULL,'Sub topic of topic 2',1,'2022-04-12 11:34:48',NULL,'2022-04-12 11:34:48',NULL),
(4,2,NULL,NULL,NULL,NULL,'Sub topic of topic 2',1,'2022-04-12 16:50:10',NULL,'2022-04-12 11:50:10',NULL),
(5,1,NULL,NULL,NULL,NULL,'aa',0,'2022-04-13 15:04:03',NULL,'2022-04-13 10:04:03',NULL);

/*Table structure for table `stp_topic` */

DROP TABLE IF EXISTS `stp_topic`;

CREATE TABLE `stp_topic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hds_book_id` int(11) DEFAULT NULL,
  `hds_sub_book_id` int(11) DEFAULT NULL,
  `volume_id` int(11) DEFAULT NULL,
  `chap_id` int(11) DEFAULT NULL,
  `topic_name` varchar(300) DEFAULT NULL,
  `is_enable` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

/*Data for the table `stp_topic` */

insert  into `stp_topic`(`id`,`hds_book_id`,`hds_sub_book_id`,`volume_id`,`chap_id`,`topic_name`,`is_enable`,`created_at`,`created_by`,`updated_at`,`updated_by`) values 
(1,NULL,NULL,NULL,NULL,'Topic 1',1,'2022-04-12 15:36:51',NULL,'2022-04-12 10:36:51',NULL),
(2,NULL,NULL,NULL,NULL,'topic 2',1,'2022-04-12 10:36:58',NULL,'2022-04-12 10:36:58',NULL);

/*Table structure for table `stp_volume` */

DROP TABLE IF EXISTS `stp_volume`;

CREATE TABLE `stp_volume` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hds_book_id` int(11) DEFAULT NULL,
  `vol_number` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_enable` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT current_timestamp(),
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4;

/*Data for the table `stp_volume` */

insert  into `stp_volume`(`id`,`hds_book_id`,`vol_number`,`is_enable`,`created_at`,`created_by`,`updated_at`,`updated_by`) values 
(7,1,'1',1,'2022-03-30 11:55:43',NULL,NULL,NULL),
(8,1,'2',1,'2022-04-11 10:43:44',NULL,'2022-04-11 10:44:12',NULL),
(9,3,'1',1,'2022-04-12 08:17:50',NULL,'2022-04-12 08:17:50',NULL),
(10,3,'2',1,'2022-04-13 06:04:06',NULL,'2022-04-13 06:04:06',NULL),
(11,3,'2',2,'2022-04-13 06:04:21',NULL,'2022-04-13 06:04:28',NULL),
(12,8,'2',1,'2022-04-18 06:18:33',NULL,'2022-04-18 06:18:33',NULL),
(13,10,'1',1,'2022-04-18 06:43:15',NULL,'2022-04-18 06:43:15',NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
