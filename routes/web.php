<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use App\Http\Controllers\SetupHadeesBookApi;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Route;


$router->get('/', function () use ($router) {
    return $router->app->version();
});
$router->get('/key', function() {
    return \Illuminate\Support\Str::random(32);
});
$router->group(['prefix' => 'api'], function () use ($router) {

    ///////////////////////////////////////// Setup Routes ////////////////////////////////////
    ///////////////////////////////////////// MODULE HADEES BOOK Routes ////////////////////////////////////
    $router->post('setupHadeesBook', ['uses' => 'SetupHadeesBookApi@addSetupHadeesBook']);
    $router->put('setupHadeesBook', ['uses' => 'SetupHadeesBookApi@updateSetupHadeesBook']);
    $router->patch('setupHadeesBook', ['uses' => 'SetupHadeesBookApi@patchDisableEnableSetupHadeesBook']);
    $router->delete('setupHadeesBook/{id}', ['uses' => 'SetupHadeesBookApi@deleteSetupHadeesBook']);
    $router->get('setupHadeesBook/{id}', ['uses' => 'SetupHadeesBookApi@getOneSetupHadeesBook']);
    $router->get('setupHadeesBook', ['uses' => 'SetupHadeesBookApi@getAllSetupHadeesBook']);

    ///////////////////////////////////////// MODULE HADEES VOLUME Routes ////////////////////////////////////
    $router->post('hadeesVolume', ['uses' => 'HadeesVolumeApi@addHadeesVolume']);
    $router->put('hadeesVolume', ['uses' => 'HadeesVolumeApi@updateHadeesVolume']);
    $router->patch('hadeesVolume', ['uses' => 'HadeesVolumeApi@patchDisableEnableHadeesVolume']);
    $router->delete('hadeesVolume/{id}', ['uses' => 'HadeesVolumeApi@deleteHadeesVolume']);
    $router->get('hadeesVolume/{id}', ['uses' => 'HadeesVolumeApi@getOneHadeesVolume']);
    $router->get('hadeesVolume', ['uses' => 'HadeesVolumeApi@getAllHadeesVolume']);

    ///////////////////////////////////////// MODULE SETUP KEYWORD Routes ////////////////////////////////////
    $router->post('setupKeyword', ['uses' => 'SetupKeywordApi@addSetupKeyword']);
    $router->put('setupKeyword', ['uses' => 'SetupKeywordApi@updateSetupKeyword']);
    $router->patch('setupKeyword', ['uses' => 'SetupKeywordApi@patchDisableEnableSetupKeyword']);
    $router->delete('setupKeyword/{id}', ['uses' => 'SetupKeywordApi@deleteSetupKeyword']);
    $router->get('setupKeyword/{id}', ['uses' => 'SetupKeywordApi@getOneSetupKeyword']);
    $router->get('setupKeyword', ['uses' => 'SetupKeywordApi@getAllSetupKeyword']);

     ///////////////////////////////////////// MODULE KEYWORD DETAILS Routes ////////////////////////////////////
     $router->post('keywordDetails', ['uses' => 'KeywordDetailsApi@addKeywordDetails']);
     $router->put('keywordDetails', ['uses' => 'KeywordDetailsApi@updateKeywordDetails']);
     $router->patch('keywordDetails', ['uses' => 'KeywordDetailsApi@patchDisableEnableKeywordDetails']);
     $router->delete('keywordDetails/{id}', ['uses' => 'KeywordDetailsApi@deleteKeywordDetails']);
     $router->get('keywordDetails/{id}', ['uses' => 'KeywordDetailsApi@getOneKeywordDetails']);
     $router->get('keywordDetails', ['uses' => 'KeywordDetailsApi@getAllKeywordDetails']);

    /////////////////////////////////////////// MODULE SETUP NARRATOR Routes ////////////////////////////////////
    $router->post('setupNarrator', ['uses' => 'SetupNarratorAPI@addSetupNarrator']);
    $router->put('setupNarrator', ['uses' => 'SetupNarratorAPI@updateSetupNarrator']);
    $router->patch('setupNarrator', ['uses' => 'SetupNarratorAPI@patchDisableEnableSetupNarrator']);
    $router->delete('setupNarrator/{id}', ['uses' => 'SetupNarratorAPI@deleteSetupNarrator']);
    $router->get('setupNarrator/{id}', ['uses' => 'SetupNarratorAPI@getOneSetupNarrator']);
    $router->get('setupNarrator', ['uses' => 'SetupNarratorAPI@getAllSetupNarrator']);

    ///////////////////////////////////////// MODULE SETUP SHARAH Routes ////////////////////////////////////
    $router->post('setupSharah', ['uses' => 'SetupSharahAPI@addSetupSharah']);
    $router->put('setupSharah', ['uses' => 'SetupSharahAPI@updateSetupSharah']);
    $router->patch('setupSharah', ['uses' => 'SetupSharahAPI@patchDisableEnableSetupSharah']);
    $router->delete('setupSharah/{id}', ['uses' => 'SetupSharahAPI@deleteSetupSharah']);
    $router->get('setupSharah/{id}', ['uses' => 'SetupSharahAPI@getOneSetupSharah']);
    $router->get('setupSharah', ['uses' => 'SetupSharahAPI@getAllSetupSharah']);

    ///////////////////////////////////////// MODULE SETUP SHAREH Routes ////////////////////////////////////
    $router->post('setupShareh', ['uses' => 'SetupSharehAPI@addSetupShareh']);
    $router->put('setupShareh', ['uses' => 'SetupSharehAPI@updateSetupShareh']);
    $router->patch('setupShareh', ['uses' => 'SetupSharehAPI@patchDisableEnableSetupShareh']);
    $router->delete('setupShareh/{id}', ['uses' => 'SetupSharehAPI@deleteSetupShareh']);
    $router->get('setupShareh/{id}', ['uses' => 'SetupSharehAPI@getOneSetupShareh']);
    $router->get('setupShareh', ['uses' => 'SetupSharehAPI@getAllSetupShareh']);

    ///////////////////////////////////////// MODULE SETUP CHAPTER Routes ////////////////////////////////////
    $router->post('setupChapter', ['uses' => 'SetupChapterApi@addSetupChapter']);
    $router->put('setupChapter', ['uses' => 'SetupChapterApi@updateSetupChapter']);
    $router->patch('setupChapter', ['uses' => 'SetupChapterApi@patchDisableEnableSetupChapter']);
    $router->delete('setupChapter/{id}', ['uses' => 'SetupChapterApi@deleteSetupChapter']);
    $router->get('setupChapter/{id}', ['uses' => 'SetupChapterApi@getOneSetupChapter']);
    $router->get('setupChapter', ['uses' => 'SetupChapterApi@getAllSetupChapter']);

    ///////////////////////////////////////// MODULE SETUP SUB CHAPTER Routes ////////////////////////////////////
    $router->post('setupSubChapter', ['uses' => 'SetupSubChapterAPI@addSetupSubChapter']);
    $router->put('setupSubChapter', ['uses' => 'SetupSubChapterAPI@updateSetupSubChapter']);
    $router->patch('setupSubChapter', ['uses' => 'SetupSubChapterAPI@patchDisableEnableSetupSubChapter']);
    $router->delete('setupSubChapter/{id}', ['uses' => 'SetupSubChapterAPI@deleteSetupSubChapter']);
    $router->get('setupSubChapter/{id}', ['uses' => 'SetupSubChapterAPI@getOneSetupSubChapter']);
    $router->get('setupSubChapter', ['uses' => 'SetupSubChapterAPI@getAllSetupSubChapter']);

    ///////////////////////////////////////// MODULE SETUP SUB TOPIC Routes ////////////////////////////////////
    $router->post('setupSubTopic', ['uses' => 'SetupSubTopicAPI@addSetupSubTopic']);
    $router->put('setupSubTopic', ['uses' => 'SetupSubTopicAPI@updateSetupSubTopic']);
    $router->patch('setupSubTopic', ['uses' => 'SetupSubTopicAPI@patchDisableEnableSetupSubTopic']);
    $router->delete('setupSubTopic/{id}', ['uses' => 'SetupSubTopicAPI@deleteSetupSubTopic']);
    $router->get('setupSubTopic/{id}', ['uses' => 'SetupSubTopicAPI@getOneSetupSubTopic']);
    $router->get('setupSubTopic', ['uses' => 'SetupSubTopicAPI@getAllSetupSubTopic']);

    ///////////////////////////////////////// MODULE SETUP TOPIC Routes ////////////////////////////////////
    $router->post('setupTopic', ['uses' => 'SetupTopicAPI@addSetupTopic']);
    $router->put('setupTopic', ['uses' => 'SetupTopicAPI@updateSetupTopic']);
    $router->patch('setupTopic', ['uses' => 'SetupTopicAPI@patchDisableEnableSetupTopic']);
    $router->delete('setupTopic/{id}', ['uses' => 'SetupTopicAPI@deleteSetupTopic']);
    $router->get('setupTopic/{id}', ['uses' => 'SetupTopicAPI@getOneSetupTopic']);
    $router->get('setupTopic', ['uses' => 'SetupTopicAPI@getAllSetupTopic']);

    ///////////////////////////////////////// MODULE SETUP TOPIC Routes ////////////////////////////////////
    $router->post('setupHadeesSubBook', ['uses' => 'SetupHadeesSubBookApi@addSetupHadeesSubBook']);
    $router->put('setupHadeesSubBook', ['uses' => 'SetupHadeesSubBookApi@updateSetupHadeesSubBook']);
    $router->patch('setupHadeesSubBook', ['uses' => 'SetupHadeesSubBookApi@patchDisableEnableSetupHadeesSubBook']);
    $router->delete('setupHadeesSubBook/{id}', ['uses' => 'SetupHadeesSubBookApi@deleteSetupHadeesSubBook']);
    $router->get('setupHadeesSubBook/{id}', ['uses' => 'SetupHadeesSubBookApi@getOneSetupHadeesSubBook']);
    $router->get('setupHadeesSubBook', ['uses' => 'SetupHadeesSubBookApi@getAllSetupHadeesSubBook']);

    ///////////////////////////////////////// MODULE SETUP TOPIC Routes ////////////////////////////////////
    $router->post('masterEntries', ['uses' => 'MasterEntriesApi@addMasterEntries']);
    $router->put('masterEntries', ['uses' => 'MasterEntriesApi@updateMasterEntries']);
    $router->patch('masterEntries', ['uses' => 'MasterEntriesApi@patchDisableEnableMasterEntries']);
    $router->delete('masterEntries/{id}', ['uses' => 'MasterEntriesApi@deleteMasterEntries']);
    $router->get('masterEntries/{id}', ['uses' => 'MasterEntriesApi@getOneMasterEntries']);
    $router->get('masterEntries', ['uses' => 'MasterEntriesApi@getAllMasterEntries']);

    ///////////////////////////////////////// MODULE SETUP TOPIC Routes ////////////////////////////////////
    $router->post('sharahSharehMapping', ['uses' => 'SharahSharehMappingApi@addSharahSharehMapping']);
    $router->put('sharahSharehMapping', ['uses' => 'SharahSharehMappingApi@updateSharahSharehMapping']);
    $router->patch('sharahSharehMapping', ['uses' => 'SharahSharehMappingApi@patchDisableEnableSharahSharehMapping']);
    $router->delete('sharahSharehMapping/{id}', ['uses' => 'SharahSharehMappingApi@deleteSharahSharehMapping']);
    $router->get('sharahSharehMapping/{id}', ['uses' => 'SharahSharehMappingApi@getOneSharahSharehMapping']);
    $router->get('sharahSharehMapping', ['uses' => 'SharahSharehMappingApi@getAllSharahSharehMapping']);

    ///////////////////////////////////////// MODULE SETUP TOPIC Routes ////////////////////////////////////
    $router->post('hadeesMuhaddisMapping', ['uses' => 'HadeesMuhaddisMappingApi@addHadeesMuhaddisMapping']);
    $router->put('hadeesMuhaddisMapping', ['uses' => 'HadeesMuhaddisMappingApi@updateHadeesMuhaddisMapping']);
    $router->patch('hadeesMuhaddisMapping', ['uses' => 'HadeesMuhaddisMappingApi@patchDisableEnableHadeesMuhaddisMapping']);
    $router->delete('hadeesMuhaddisMapping/{id}', ['uses' => 'HadeesMuhaddisMappingApi@deleteHadeesMuhaddisMapping']);
    $router->get('hadeesMuhaddisMapping/{id}', ['uses' => 'HadeesMuhaddisMappingApi@getOneHadeesMuhaddisMapping']);
    $router->get('hadeesMuhaddisMapping', ['uses' => 'HadeesMuhaddisMappingApi@getAllHadeesMuhaddisMapping']);

    ///////////////////////////////////////// MODULE SHARAH ENTRY Routes ////////////////////////////////////
    $router->post('sharahEntry', ['uses' => 'SharahEntryApi@addSharahEntry']);
    $router->put('sharahEntry', ['uses' => 'SharahEntryApi@updateSharahEntry']);
    $router->patch('sharahEntry', ['uses' => 'SharahEntryApi@patchDisableEnableSharahEntry']);
    $router->delete('sharahEntry/{id}', ['uses' => 'SharahEntryApi@deleteSharahEntry']);
    $router->get('sharahEntry/{id}', ['uses' => 'SharahEntryApi@getOneSharahEntry']);
    $router->get('sharahEntry', ['uses' => 'SharahEntryApi@getAllSharahEntry']);

    ///////////////////////////////////////// MODULE SETUP TOPIC Routes ////////////////////////////////////
    $router->post('setupLanguage', ['uses' => 'SetupLanguageApi@addSetupLanguage']);
    $router->put('setupLanguage', ['uses' => 'SetupLanguageApi@updateSetupLanguage']);
    $router->patch('setupLanguage', ['uses' => 'SetupLanguageApi@patchDisableEnableSetupLanguage']);
    $router->delete('setupLanguage/{id}', ['uses' => 'SetupLanguageApi@deleteSetupLanguage']);
    $router->get('setupLanguage/{id}', ['uses' => 'SetupLanguageApi@getOneSetupLanguage']);
    $router->get('setupLanguage', ['uses' => 'SetupLanguageApi@getAllSetupLanguage']);

    ///////////////////////////////////////// MODULE SETUP TOPIC Routes ////////////////////////////////////
    $router->post('setupMuhaddiseen', ['uses' => 'SetupMuhaddisApi@addSetupMuhaddis']);
    $router->put('setupMuhaddiseen', ['uses' => 'SetupMuhaddisApi@updateSetupMuhaddis']);
    $router->patch('setupMuhaddiseen', ['uses' => 'SetupMuhaddisApi@patchDisableEnableSetupMuhaddis']);
    $router->delete('setupMuhaddiseen/{id}', ['uses' => 'SetupMuhaddisApi@deleteSetupMuhaddis']);
    $router->get('setupMuhaddiseen/{id}', ['uses' => 'SetupMuhaddisApi@getOneSetupMuhaddis']);
    $router->get('setupMuhaddiseen', ['uses' => 'SetupMuhaddisApi@getAllSetupMuhaddis']);

});

